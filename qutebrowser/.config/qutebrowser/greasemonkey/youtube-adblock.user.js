// ==UserScript==
// @name         Auto Skip YouTube Ads
// @version      1.0.2
// @description  Speed up and skip YouTube ads automatically
// @author       codiac-killer
// @match        *://*.youtube.com/*
// @exclude      *://*.youtube.com/subscribe_embed?*
// ==/UserScript==
const skipAds = () => {
    // Get skip button and click it
    const btn = document
        .getElementsByClassName("ytp-ad-skip-button ytp-button")
        .item(0);
    if (btn) {
        btn.click();
    }

    document.querySelector("video").style.visibility = "visible";
    // (unskipable ads) If skip button didn't exist / was not clicked speed up video
    const ad = [...document.querySelectorAll(".ad-showing")][0];
    console.log(ad);
    if (ad) {
        // Speed up and mute
        document.querySelector("video").playbackRate = 16;
        document.querySelector("video").muted = true;
        document.querySelector("video").style.visibility = "hidden";
    }
};
const main = new MutationObserver(skipAds);
main.observe(
    document.getElementsByClassName("video-ads ytp-ad-module").item(0),
    { attributes: true, characterData: true, childList: true },
);
skipAds();
// setTimeout(skipAds, 100);

// remove shodow
// document.querySelector(".ytp-gradient-bottom").style.background = "transparent";
document.querySelector(".ytp-gradient-bottom").style.display = "none";

// remove ads boxes
[...document.querySelectorAll(".ytd-companion-slot-renderer")].forEach((i) => {
    i.style.display = "none";
});
