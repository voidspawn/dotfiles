set fish_greeting ""

set -gx TERM xterm-256color


# theme
set -g theme_color_scheme terminal-dark
set -g fish_prompt_pwd_dir_length 1
set -g theme_display_user yes
set -g theme_hide_hostname no
set -g theme_hostname always

# TokyoNight Color Palette
set -l foreground c0caf5
set -l selection 33467c
set -l comment 565f89
set -l red f7768e
set -l orange ff9e64
set -l yellow e0af68
set -l green 9ece6a
set -l purple 9d7cd8
set -l cyan 7dcfff
set -l pink bb9af7

# Syntax Highlighting Colors
set -g fish_color_normal $foreground
set -g fish_color_command $cyan
set -g fish_color_keyword $pink
set -g fish_color_quote $yellow
set -g fish_color_redirection $foreground
set -g fish_color_end $orange
set -g fish_color_error $red
set -g fish_color_param $purple
set -g fish_color_comment $comment
set -g fish_color_selection --background=$selection
set -g fish_color_search_match --background=$selection
set -g fish_color_operator $green
set -g fish_color_escape $pink
set -g fish_color_autosuggestion $comment

# Completion Pager Colors
set -g fish_pager_color_progress $comment
set -g fish_pager_color_prefix $cyan
set -g fish_pager_color_completion $foreground
set -g fish_pager_color_description $comment


set -gx GTK_THEME Breeze:dark
set -gx STOW_DIR $HOME/.dotfiles
set -gx EDITOR nvim
set -gx SUDO_EDITOR nvim
set -gx TERMINAL ghostty
set -gx BROWSER firefox
set -gx MANPAGER "nvim +Man!"

set -qx QT_STYLE_OVERRIDE kvantum
# set -qx QT_QPA_PLATFORMTHEME qt6ct
set -gx CM_LAUNCHER rofi

set -gx XDG_CONFIG_HOME $HOME/.config
set -gx XDG_DATA_HOME $HOME/.local/share
set -gx XDG_CACHE_HOME $HOME/.cache
set -gx XDG_STATE_HOME $HOME/.local/state

set -gx GTK2_RC_FILES $XDG_CONFIG_HOME/gtk-2.0/gtkrc
set -gx WGETRC $XDG_CONFIG_HOME/wget/wgetrc
# set -gx INPUTRC $XDG_CONFIG_HOME/shell/inputrc
set -gx ZDOTDIR $XDG_CONFIG_HOME/zsh
set -gx WINEPREFIX $XDG_DATA_HOME/wineprefixes/default
set -gx KODI_DATA $XDG_DATA_HOME/kodi
set -gx PASSWORD_STORE_DIR  $XDG_DATA_HOME/password-store
set -gx TMUX_TMPDIR $XDG_RUNTIME_DIR
set -gx ANDROID_SDK_HOME $XDG_CONFIG_HOME/android
set -gx ANSIBLE_CONFIG $XDG_CONFIG_HOME/ansible/ansible.cfg
set -gx UNISON $XDG_DATA_HOME/unison
set -gx HISTFILE $XDG_DATA_HOME/history
set -gx WEECHAT_HOME $XDG_CONFIG_HOME/weechat
set -gx MBSYNCRC $XDG_CONFIG_HOME/mbsync/config
set -gx ELECTRUMDIR $XDG_DATA_HOME/electrum

set -gx GNUPGHOME $XDG_DATA_HOME/gnupg
set -gx KDEHOME $XDG_CONFIG_HOME/kde
# set -gx LESSHISTFILE "-"
set -gx LESSHISTFILE $XDG_CACHE_HOME/less/history
set -gx MPLAYER_HOME $XDG_CONFIG_HOME/mplayer

set -gx XINITRC $XDG_CONFIG_HOME/X11/xinitrc
set -gx _Z_DATA $XDG_DATA_HOME/z





set -gx PATH bin $PATH
set -gx PATH ~/bin $PATH
set -gx PATH ~/.local/bin $PATH
#  mkdir -p "$HOME/.local/share/npm/bin"
# zsh: npm config set prefix "${HOME}/.local/share/npm"
# fish: npm config set prefix "$HOME/.local/share/npm"
# check: npm config get prefix

# Deno
set -gx DENO_INSTALL ~/.deno
set -gx PATH $DENO_INSTALL/bin $PATH



# NodeJS
set -gx PATH ~/.local/share/npm/bin $PATH
# set -gx PATH node_modules/.bin $PATH
set -gx NODE_OPTIONS "--max-old-space-size=8192 --disable-warning=ExperimentalWarning"

#npm set this
#npm config set prefix=${XDG_DATA_HOME}/npm cache=${XDG_CACHE_HOME}/npm init-module=${XDG_CONFIG_HOME}/npm/config/npm-init.js
set -gx NPM_CONFIG_USERCONFIG $XDG_CONFIG_HOME/npm/npmrc

# Java
set -gx _JAVA_OPTIONS '-Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java'

# Go
set -gx GOPATH $XDG_DATA_HOME/go
# set -g GOPATH $HOME/go
set -gx PATH $GOPATH/bin $PATH

# Rust
set -gx CARGO_HOME $XDG_DATA_HOME/cargo
set -gx PATH $CARGO_HOME/bin $PATH


# ssh
set -gx SUDO_ASKPASS /usr/bin/lxqt-openssh-askpass &





if not pgrep --full ssh-agent | string collect > /dev/null
    eval (ssh-agent -c) > /dev/null
    set -Ux SSH_AGENT_PID $SSH_AGENT_PID
    set -Ux SSH_AUTH_SOCK $SSH_AUTH_SOCK
end



set -Ux LF_ICONS "\
*.7z=:\
*.aac=:\
*.ace=:\
*.alz=:\
*.arc=:\
*.arj=:\
*.asf=:\
*.atom=:\
*.au=:\
*.avi=:\
*.bash=:\
*.bash_history=:\
*.bashprofile=:\
*.bashrc=:\
*.bmp=:\
*.bz2=:\
*.bz=:\
*.c=:\
*.cab=:\
*.cc=:\
*.cfg=:\
*.cgm=:\
*.clang-format=:\
*.clj=:\
*.cmd=:\
*.coffee=:\
*.cpio=:\
*.cpp=:\
*.css=:\
*.d=:\
*.dart=:\
*.deb=:\
*.dl=:\
*.DS_Store=:\
*.dwm=:\
*.dz=:\
*.ear=:\
*.emf=:\
*.env=:\
*.erl=:\
*.esd=:\
*.exs=:\
*.fish=:\
*.flac=:\
*.flc=:\
*.fli=:\
*.flv=:\
*.fs=:\
*.gif=:\
*.git=:\
*.gitattributes=:\
*.gitconfig=:\
*.github=:\
*.gitignore=:\
*.gitignore_global=:\
*.gitkeep=:\
*.gitmodules=:\
*.gl=:\
*.go=:\
*.gz=:\
*.h=:\
*.hh=:\
*.hidden=:\
*.hpp=:\
*.hs=:\
*.html=:\
*.hyper.js=:\
*.jar=:\
*.java=:\
*.jl=:\
*.jpeg=:\
*.jpg=:\
*.js=:\
*.json=:\
*.jsx=:\
*.lha=:\
*.lrz=:\
*.lua=:\
*.lz4=:\
*.lz=:\
*.lzh=:\
*.lzma=:\
*.lzo=:\
*.m2v=:\
*.m4a=:\
*.m4v=:\
*.map=:\
*.md=:\
*.mdx=:\
*.mid=:\
*.midi=:\
*.mjpeg=:\
*.mjpg=:\
*.mka=:\
*.mkv=:\
*.mng=:\
*.mov=:\
*.mp3=:\
*.mp4=:\
*.mp4v=:\
*.mpc=:\
*.mpeg=:\
*.mpg=:\
*.nix=:\
*.npmignore=:\
*.npmrc=:\
*.nuv=:\
*.nvmrc=:\
*.oga=:\
*.ogg=:\
*.ogm=:\
*.ogv=:\
*.ogx=:\
*.opus=:\
*.pbm=:\
*.pcx=:\
*.pdf=:\
*.pgm=:\
*.php=:\
*.pl=:\
*.png=:\
*.ppm=:\
*.pro=:\
*.ps1=:\
*.py=:\
*.qt=:\
*.ra=:\
*.rar=:\
*.rb=:\
*.rm=:\
*.rmvb=:\
*.rpm=:\
*.rs=:\
*.rvm=:\
*.rz=:\
*.sar=:\
*.scala=:\
*.sh=:\
*.skhdrc=:\
*.sol=ﲹ:\
*.spx=:\
*.svg=:\
*.svgz=:\
*.swm=:\
*.t7z=:\
*.tar=:\
*.taz=:\
*.tbz2=:\
*.tbz=:\
*.tga=:\
*.tgz=:\
*.tif=:\
*.tiff=:\
*.tlz=:\
*.tmux.conf=:\
*.trash=:\
*.ts=:\
*.tsx=:\
*.txz=:\
*.tz=:\
*.tzo=:\
*.tzst=:\
*.vim=:\
*.vimrc=:\
*.vob=:\
*.vscode=:\
*.war=:\
*.wav=:\
*.webm=:\
*.wim=:\
*.xbm=:\
*.xcf=:\
*.xpm=:\
*.xspf=:\
*.xwd=:\
*.xz=:\
*.yabairc=:\
*.yaml=פּ:\
*.yarn-integrity=:\
*.yarnrc=:\
*.yml=פּ:\
*.yuv=:\
*.z=:\
*.zip=:\
*.zoo=:\
*.zprofile=:\
*.zprofile=:\
*.zsh=:\
*.zsh_history=:\
*.zshrc=:\
*.zst=:\
*bin=:\
*config=:\
*docker-compose.yml=:\
*dockerfile=:\
*gradle=:\
*gruntfile.coffee=:\
*gruntfile.js=:\
*gruntfile.ls=:\
*gulpfile.coffee=:\
*gulpfile.js=:\
*gulpfile.ls=:\
*include=:\
*lib=:\
*localized=:\
*node_modules=:\
*package.json=:\
*rubydoc=:\
*tsconfig.json=:\
*yarn.lock=:\
di=:\
dt=:\
ex=:\
fi=:\
ln=:\
or=:\
ow=:\
st=:\
tw=:\
"






# aliases

# to run the normal ls just use \ls
alias t '/usr/bin/time --format="Time: %es"'
alias l "exa -al --color=always --group-directories-first --icons --git"
alias la "exa -a --color=always --group-directories-first --icons --git"
alias ll "exa -l --color=always --group-directories-first --icons --git"
alias lt "exa -aT --color=always --group-directories-first --icons --git"
alias lh 'exa -a | egrep "^\."'
alias md "mkdir -p"
alias e "$EDITOR"
alias ee "NVIM_APPNAME=nvimnext nvim"
alias c "clear"
alias q "exit"
alias :q "exit"
alias nstart "nvim --listen ~/.cache/nvim/server.pipe"
alias nstop "nvim --server ~/.cache/nvim/server.pipe --remote-send '<C-\><C-N>:wqa<CR>'"
alias nremote "nvim --server ~/.cache/nvim/server.pipe --remote"
alias p "sudo pacman"
alias s "kitty +kitten ssh"
#alias cleanup="sudo pacman -Rns $(pacman -Qtdq)"
alias checkup "pacman -Qtdq"
alias SS "sudo systemctl"
alias ka "killall"
alias g "git"
alias glg "git log --graph --all --oneline"
alias gco "git checkout"
alias gbr "git branch"
alias gci "git commit -m"
alias gst "git status"
alias gpl "git pull"
alias gph "git push"
alias gpho "git push -u origin HEAD"
alias gdw "git diff -w"
alias gdpl "git diff -w @{1}"
alias guci "git reset --soft HEAD~1"
alias ga "git add"
alias gad "git add"
alias gap "git add -p"
alias gdev "git checkout dev"
alias ssha 'eval $(ssh-agent) > /dev/null && ssh-add'
alias nrd "npm run dev"
alias nrb "npm build"
alias nrt "npm test"
alias f "lf"
alias lsp "pacman -Qett --color=always | less"
# alias php "php74"
#alias pdeps "pacman -Qi | sed '/^Depends On/,/^Required By/{ s/^Required By.*$//; H; d }; /^Name/!d; /^Name/{ n;x;}'| sed '/^$/s//==================================================================================/'"
alias selenium "java -jar ~/Applications/selenium-server-standalone-3.141.59.jar -port 4444"
alias please 'sudo !!'
alias plz 'sudo !!'
alias fuck 'sudo !!'
alias xup 'xrdb .Xresources'
alias srcrc 'source ~/.zshrc'
alias srcpf 'source ~/.zprofile'
alias shtop 'sudo htop'
alias grep 'grep --color=auto'
alias .. 'cd ..'
alias ... 'cd ../..'
alias .... 'cd ../../..'
alias rm 'rm -i'
alias cp 'cp -i'
alias mv 'mv -i'
alias cal 'gcal --starting-day=1'
alias weather 'curl v2.wttr.in'
alias dosbox 'dosbox -conf "$XDG_CONFIG_HOME"/dosbox/dosbox.conf'
alias wget 'wget --hsts-file="$XDG_DATA_HOME/wget-hsts"'
#alias hq $'history | awk \'{first = $1;$1="";print $0;}\' | sort | uniq -c | sort -nr'

bind -M insert \ca beginning-of-line
bind -M insert \ce end-of-line

function sudo --description "Replacement for Bash 'sudo !!' command to run last command using sudo."
    if test "$argv" = !!
        echo sudo $history[1]
        eval command sudo $history[1]
    else
        command sudo $argv
    end
end

function y
    set tmp (mktemp -t "yazi-cwd.XXXXXX")
    yazi $argv --cwd-file="$tmp"
    if set cwd (command cat -- "$tmp"); and [ -n "$cwd" ]; and [ "$cwd" != "$PWD" ]
        builtin cd -- "$cwd"
    end
    rm -f -- "$tmp"
end


function on_fish_bind_mode --on-variable fish_bind_mode
    # export the vi_mode_symbol variable which Starship can use
    set --global --export vi_mode_symbol ""

    # Do whatever you want here to set vi_mode_symbol...
    set --local color
    set --local char
    if test "$fish_key_bindings" = fish_vi_key_bindings
        switch $fish_bind_mode
            case default
                set color red
                set symbol N
            case insert
                set color green
                set symbol I
            case replace replace_one
                set color green
                set symbol R
            case visual
                set color brmagenta
                set symbol V
            case '*'
                set color cyan
                set symbol "?"
        end
        set vi_mode_symbol (set_color --bold $color)"[$symbol]"(set_color normal)
    end
end

if status is-interactive
    # Commands to run in interactive sessions can go here
set fish_vi_force_cursor 1
  # I'm trying to grow a neckbeard
  # fish_vi_key_bindings
  # Set the cursor shapes for the different vi modes.
  set fish_cursor_default     block      blink
  set fish_cursor_insert      line       blink
  set fish_cursor_replace_one underscore blink
  set fish_cursor_visual      block

  function fish_user_key_bindings
    # Execute this once per mode that emacs bindings should be used in
    fish_default_key_bindings -M insert
    fish_vi_key_bindings --no-erase insert
  end
end

if test -f /usr/share/autojump/autojump.fish
 source /usr/share/autojump/autojump.fish
end

if test -f ~/.local/share/cargo/env.fish
 source ~/.local/share/cargo/env.fish
end

# Set up fzf key bindings
fzf --fish | source

set -g STARSHIP_CONFIG $HOME/.config/starship/starship.toml
starship init fish | source

zoxide init fish | source

# if status is-interactive
# and not set -q TMUX
#     exec tmux new-session -A -s main
# end
