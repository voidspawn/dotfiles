function fish_prompt
    set -g __fish_git_prompt_showupstream auto

    echo (set_color blue)(prompt_pwd)(set_color purple)(fish_git_prompt) (set_color green)'❯'


    set_color normal
end
