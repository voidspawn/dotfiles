# interpreter for shell commands
set shell sh

# set '-eu' options for shell commands
# These options are used to have safer shell commands. Option '-e' is used to
# exit on error and option '-u' is used to give error for unset variables.
# Option '-f' disables pathname expansion which can be useful when $f, $fs, and
# $fx variables contain names with '*' or '?' characters. However, this option
# is used selectively within individual commands as it can be limiting at
# times.
set shellopts '-eu'

# set internal field separator (IFS) to "\n" for shell commands
# This is useful to automatically split file names in $fs and $fx properly
# since default file separator used in these variables (i.e. 'filesep' option)
# is newline. You need to consider the values of these options and create your
# commands accordingly.
set ifs "\n"

# leave some space at the top and the bottom of the screen
set scrolloff 10

# preview images, videos, text
set preview true
set previewer ~/.config/lf/preview
set cleaner ~/.config/lf/lf_kitty_clean

# show hidden files
set hidden true

# show icons
set icons true

# ignore case
set ignorecase true

# enable mouse
set mouse true

# line number
# set number true
# set relativenumber true

# use enter for shell commands
map <enter> shell

# execute current file (must be executable)
map x $$f
map X !$f

# dedicated keys for file opener actions
cmd mimetype %file --mime-type -bL "$f"
map o &mimeopen $f
map O $mimeopen --ask $f

# define a custom 'open' command
# This command is called when current file is not a directory. You may want to
# use either file extensions and/or mime types here. Below uses an editor for
# text files and a file opener for the rest.
cmd open ${{
    test -L $f && f=$(readlink -f $f)
    case $(file --mime-type $f -b) in
        application/vnd.openxmlformats-officedocument.spreadsheetml.sheet) localc $fx ;;
        image/vnd.djvu|application/pdf|application/octet-stream|application/postscript) setsid -f zathura $fx >/dev/null 2>&1 ;;
        text/*|application/json|inode/x-empty) $EDITOR $fx;;
        image/x-xcf) setsid -f gimp $f >/dev/null 2>&1 ;;
        image/svg+xml) display -- $f ;;
        image/*) rotdir $f | grep -i "\.\(png\|jpg\|jpeg\|gif\|webp\|tif\|ico\)\(_large\)*$" |
            setsid -f sxiv -aio 2>/dev/null | while read -r file; do
                [ -z "$file" ] && continue
                lf -remote "send select \"$file\""
                lf -remote "send toggle"
              done &
              ;;
        audio/*) mpv --audio-display=no $f ;;
        video/*) setsid -f mpv $f -quiet >/dev/null 2>&1 ;;
        application/pdf|application/vnd*|application/epub*) setsid -f zathura $fx >/dev/null 2>&1 ;;
        application/pgp-encrypted) $EDITOR $fx ;;
        *) for f in $fx; do setsid $OPENER $f > /dev/null 2> /dev/null & done;;
    esac
}}

# make folder
cmd mkdir ${{
  printf "Directory Name: "
  read ans
  mkdir $ans
}}

# create file
cmd mkfile ${{
  printf "File Name: "
  read ans
  touch $ans
}}

# create file as root
cmd sudomkfile ${{
  printf "File Name: "
  read ans
  sudo $EDITOR $ans
}}

# chmod
cmd chmod ${{
  printf "Mode Bits: "
  read ans

  for file in "$fx"
  do
    chmod $ans $file
  done

  lf -remote 'send reload'
}}

# define a custom 'rename' command without prompt for overwrite
# cmd rename %[ -e $1 ] && printf "file exists" || mv $f $1
# map r push :rename<space>

# make sure trash folder exists
# %mkdir -p ~/.trash

# move current file or selected files to trash folder
# (also see 'man mv' for backup/overwrite options)
cmd trash2 %set -f; mv $fx ~/.trash
#cmd trash %trash-put $fx

# Trash cli bindings - package rmtrash trash-cli is needed
cmd trash1 ${{
  files=$(printf "$fx" | tr '\n' ';')
  while [ "$files" ]; do
    # extract the substring from start of string up to delimiter.
    # this is the first "element" of the string.
    file=${files%%;*}

    trash-put "$(basename "$file")"
    # if there's only one element left, set `files` to an empty string.
    # this causes us to exit this `while` loop.
    # else, we delete the first "element" of the string from files, and move onto the next.
    if [ "$files" = "$file" ]; then
      files=''
    else
      files="${files#*;}"
    fi
  done
}}

cmd clear_trash %trash-empty

cmd restore_trash ${{
  trash-restore
}}

#cmd clear_trash %trash-empty
# define a custom 'delete' command
# cmd delete ${{
#     set -f
#     printf "$fx\n"
#     printf "delete?[y/n]"
#     read ans
#     [ $ans = "y" ] && rm -rf $fx
# }}
cmd delete ${{
    clear; tput cup $(($(tput lines)/3)); tput bold
    set -f
    printf "%s\n\t" "$fx"
    printf "delete?[y/n]"
    read ans
    [ $ans = "y" ] && rm -rf -- $fx
}}

# use '<delete>' key for either 'trash' or 'delete' command
#map <delete> trash
map <delete> delete

cmd moveto ${{
    clear; tput cup $(($(tput lines)/3)); tput bold
    set -f
    clear; echo "Move to where?"
    dest="$(sed -e 's/\s*#.*//' -e '/^$/d' -e 's/^\S*\s*//' ${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-dirs | fzf | sed 's|~|$HOME|')" &&
    for x in $fx; do
        eval mv -iv \"$x\" \"$dest\"
    done &&
    notify-send "🚚 File(s) moved." "File(s) moved to $dest."
}}

cmd copyto ${{
    clear; tput cup $(($(tput lines)/3)); tput bold
    set -f
    clear; echo "Copy to where?"
    dest="$(sed -e 's/\s*#.*//' -e '/^$/d' -e 's/^\S*\s*//' ${XDG_CONFIG_HOME:-$HOME/.config}/shell/bm-dirs | fzf | sed 's|~|$HOME|')" &&
    for x in $fx; do
        eval cp -ivr \"$x\" \"$dest\"
    done &&
    notify-send "📋 File(s) copied." "File(s) copies to $dest."
}}

#cmd paste &{{
#    set -- $(lf -remote load)
#    mode="$1"
#    shift
#    case "$mode" in
#        copy)
#            rsync -av --ignore-existing --progress -- "$@" . |
#            stdbuf -i0 -o0 -e0 tr '\r' '\n' |
#            while IFS= read -r line; do
#                lf -remote "send $id echo $line"
#            done
#            ;;
#        move) mv -n -- "$@" .;;
#    esac
#    lf -remote "send load"
#    lf -remote "send clear"
#}}
# extract the current file with the right command
# (xkcd link: https://xkcd.com/1168/)
cmd extract ${{
    set -f
    case $f in
        *.tar.bz|*.tar.bz2|*.tbz|*.tbz2) tar xjvf $f;;
        *.tar.gz|*.tgz) tar xzvf $f;;
        *.tar.xz|*.txz) tar xJvf $f;;
        *.zip) unzip $f;;
        *.rar) unrar x $f;;
        *.7z) 7z x $f;;
        *) echo "Unsupported format" ;;
    esac
}}
#cmd extract ${{
#    clear; tput cup $(($(tput lines)/3)); tput bold
#    set -f
#    printf "%s\n\t" "$fx"
#    printf "extract?[y/N]"
#    read ans
#    [ $ans = "y" ] && aunpack $fx
#}}

# compress current file or selected files with tar and gunzip
# cmd tar ${{
#     set -f
#     mkdir $1
#     cp -r $fx $1
#     tar czf $1.tar.gz $1
#     rm -rf $1
# }}

# compress current file or selected files with zip
# cmd zip ${{
#     set -f
#     mkdir $1
#     cp -r $fx $1
#     zip -r $1.zip $1
#     rm -rf $1
# }}
cmd zip %zip -r "$f" "$f"
cmd tar %tar cvf "$f.tar" "$f"
cmd targz %tar cvzf "$f.tar.gz" "$f"
cmd tarbz2 %tar cjvf "$f.tar.bz2" "$f"

# Quick folder navigation with fasd
cmd fasd_dir ${{
   res="$(fasd -dl | grep -iv cache | fzf 2>/dev/tty | sed 's/\\/\\\\/g;s/"/\\"/g')"
   if [ -d "$res" ]; then
      cmd="cd"
   else
      cmd="select"
   fi
 lf -remote "send $id $cmd \"$res\""
 }}

map go :fasd_dir

cmd runmedia ${{
    lf -remote "send $id cd \"/run/media/$USER\""
}}

cmd git_branch ${{
    git branch | fzf | xargs git checkout
    pwd_shell=$(pwd)
    lf -remote "send $id updir"
    lf -remote "send $id cd \"$pwd_shell\""
}}
map gb :git_branch
map gp ${{clear; git pull --rebase || true; echo "press ENTER"; read ENTER}}
map gs ${{clear; git status; echo "press ENTER"; read ENTER}}
map gL ${{clear; git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit}}

map gh cd ~
map gv cd ~/Videos
map gi cd ~/Pictures
map gd cd ~/Documents
map gD cd ~/Downloads
map ge cd ~/Desktop
map gc cd ~/.config
map gl cd ~/.local
map gE cd /etc
map gu :runmedia
# map gu cd /run/media/$USER
map gU cd /usr/share

# binding
map <esc> cmd-escape
map <tab> cmd-complete

map <enter> open
map . set hidden!
map DD delete
map am mimetype
map af mkfile
map ar sudomkfile
map ad mkdir
map ah chmod
# Archive Mappings
map az zip
map at tar
map ag targz
map ab targz
map au extract

cmd fzf_jump ${{
    res="$(find . -maxdepth 1 | fzf --reverse --header='Jump to location' | sed 's/\\/\\\\/g;s/"/\\"/g')"
    if [ -d "$res" ]; then
        cmd="cd"
    else
        cmd="select"
    fi
    lf -remote "send $id $cmd \"$res\""
}}
map f :fzf_jump

cmd fzf_search ${{
    res="$( \
        RG_PREFIX="rg --column --line-number --no-heading --color=always \
            --smart-case "
        FZF_DEFAULT_COMMAND="$RG_PREFIX ''" \
            fzf --bind "change:reload:$RG_PREFIX {q} || true" \
            --ansi --layout=reverse --header 'Search in files' \
            | cut -d':' -f1
    )"
    [ ! -z "$res" ] && lf -remote "send $id select \"$res\""
}}
map gf :fzf_search

cmd dragon %dragon-drop -a -x $fx
cmd dragon-stay %dragon-drop -a $fx
cmd dragon-individual %dragon-drop $fx
cmd cpdragon %cpdragon
cmd mvdragon %mvdragon
cmd dlfile %dlfile

# drag and drop
map bd dragon
map bs dragon-stay
map bi dragon-individual
map bc cpdragon
map bm mvdragon
map bf dlfile

# mouse control
map <m-up> up
map <m-down> down
map <m-1> open
map <m-2> updir
