# for a secute sh, make my own Package manager
plug () {
if [ -f "$ZDOTDIR/plugins/$1/$1.plugin.zsh" ] ; then
    source "$ZDOTDIR/plugins/$1/$1.plugin.zsh"
fi
}

plug "zsh-autosuggestions"
plug "supercharge"
plug "exa"
plug "zsh-syntax-highlighting"
plug "fzf"
plug "zsh-vi-mode"

# zsh-completions
fpath+="$ZDOTDIR/plugins/zsh-completions/src"

# Load and initialise completion system
autoload -Uz compinit
compinit

# beeping is annoying
unsetopt BEEP


# Variables {{{

# XDG Variables
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_STATE_HOME="$HOME/.local/state"

# Applications
export STOW_DIR="$HOME/.dotfiles"
export TERM="xterm-256color"
export EDITOR="nvim"
export SUDO_EDITOR="nvim"
export TERMINAL="ghostty"
export BROWSER="firefox"
# export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export MANPAGER="nvim +Man!"
export CM_LAUNCHER="rofi"

export WGETRC="${XDG_CONFIG_HOME:-$HOME/.config}/wget/wgetrc"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export WINEPREFIX="${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes/default"
export ANDROID_SDK_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/android"
export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
export ANSIBLE_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/ansible/ansible.cfg"
export ELECTRUMDIR="${XDG_DATA_HOME:-$HOME/.local/share}/electrum"
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export KDEHOME="$XDG_CONFIG_HOME"/kde
export MPLAYER_HOME="$XDG_CONFIG_HOME"/mplayer
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc

# THEMES
export GTK_THEME="Breeze:dark"
export QT_STYLE_OVERRIDE="kvantum"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"

# ssh
export SUDO_ASKPASS="/usr/bin/ssh-askpass sudo -A sleep 10 &"
if [[ -z "${SSH_AUTH_SOCK}" ]]; then
    export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
fi

# /Variables }}}

# Paths {{{

# set History path
if [ -f "~/.cache/zsh/history" ] ; then
    HISTFILE=~/.cache/zsh/history
fi

# bin paths
if [ -d "$HOME/.bin" ] ; then
    PATH="$HOME/.bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi
if [ -d "$HOME/Applications" ] ;
then PATH="$HOME/Applications:$PATH"
fi
if [ -d "/var/lib/flatpak/exports/bin/" ] ;
then PATH="/var/lib/flatpak/exports/bin/:$PATH"
fi

# if go is installed
if [ -f "/usr/bin/go" ] ; then
    export GOPATH=$HOME/go
fi
if [ -d "/usr/local/go/bin" ] ; then
    PATH="/usr/local/go/bin:$PATH"
fi
if [ -d "$GOROOT/bin" ] ; then
    PATH="$GOROOT/bin:$PATH"
fi
if [ -d "$GOPATH/bin" ] ; then
    PATH="$GOPATH/bin:$PATH"
fi

# Node paths
if [ -d "$HOME/.config/yarn/global/node_modules/.bin" ] ; then
    PATH="$HOME/.config/yarn/global/node_modules/.bin:$PATH"
fi

# Rust paths
if [ -d "$HOME/.local/share/cargo/bin" ] ; then
    PATH="$HOME/.local/share/cargo/bin:$PATH"
fi

# /Paths }}}

# Alias {{{

# Defaut Applications
alias f="yazi"
alias e="$EDITOR"
alias -s {js,json}=nvim
alias -s {c,h}=nvim
alias -s {rs,toml}=nvim
alias -s zsh=nvim
alias -s vim=nvim
alias -s txt=nvim
alias -s {md,MD}=nvim

# Useful alias
alias t='/usr/bin/time --format="Time: %es"'
alias c="clear"
alias please='sudo !!'
alias grep='grep --color=auto'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias cal='gcal --starting-day=1'
alias hq=$'history | awk \'{first = $1;$1="";print $0;}\' | sort | uniq -c | sort -nr'
alias dosbox=dosbox -conf "$XDG_CONFIG_HOME"/dosbox/dosbox.conf
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"

# quit
alias q="exit"
alias :q="exit"

# Arch
alias p="sudo pacman"
alias checkup="pacman -Qtdq"
alias lsp="pacman -Qett --color=always | less"
alias pdeps="pacman -Qi | sed '/^Depends On/,/^Required By/{ s/^Required By.*$//; H; d }; /^Name/!d; /^Name/{ n;x;}'| sed '/^$/s//==================================================================================/'"

# ssh
alias s="kitty +kitten ssh"
alias ssha='eval $(ssh-agent) > /dev/null && ssh-add'

# ls
alias l="exa -al --color=always --group-directories-first --icons --git"
alias la="exa -a --color=always --group-directories-first --icons --git"
alias ll="exa -l --color=always --group-directories-first --icons --git"
alias lt="exa -aT --color=always --group-directories-first --icons --git"
alias lh='exa -a | egrep "^\."'

# nvim
alias nstart="nvim --listen ~/.cache/nvim/server.pipe"
alias nstop="nvim --server ~/.cache/nvim/server.pipe --remote-send '<C-\><C-N>:wqa<CR>'"
alias nremote="nvim --server ~/.cache/nvim/server.pipe --remote"

# make directory
alias md="mkdir -p"

# git
alias g="git"
alias gco="git checkout"
alias gbr="git branch"
alias gci="git commit --verbose -m"
alias gst="git status"
alias gpl="git pull"
alias gph="git push"
alias gpho="git push -u origin HEAD"
alias gdw="git diff -w"
alias gdpl="git diff -w @{1}"
alias guci="git reset --soft HEAD~1"
alias ga="git add"
alias gap="git add -p"
alias gsave="git stash --all"
alias qload="git stash pop"

# pipe command
alias -g G="| grep -i"


# /Alias }}}

# Functions {{{

fdiff () {
    diff -u --color=always -- "${1}" "${2}" | less -R
}

vdiff () {
    if [ "${#}" -ne 2 ] ; then
        echo "vdiff requires two arguments"
        echo "  comparing dirs:  vdiff dir_a dir_b"
        echo "  comparing files: vdiff file_a file_b"
        return 1
    fi

    local left="${1}"
    local right="${2}"

    if [ -d "${left}" ] && [ -d "${right}" ]; then
        nvim +"DirDiff ${left} ${right}"
    else
        nvim -d "${left}" "${right}"
    fi
}

pause() {
    read "Press any key to continue . . ."
}

word() {
    rg $1 /usr/share/dict/words | fzf | pbcopy
}

# fkill - kill processes - list only the ones you can kill.
fkill() {
    local pid
    if [ "$UID" != "0" ]; then
        pid=$(ps -f -u $UID | sed 1d | fzf -m | awk '{print $2}')
    else
        pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')
    fi

    if [ "x$pid" != "x" ]
    then
        echo $pid | xargs kill -${1:-9}
    fi
}
fvh() {
    rg "$1" --ignore-case --files-with-matches --no-messages ~/Projects/ ~/.config/nvim/ | fzf --preview "highlight -O ansi -l {} 2> /dev/null | rg --colors 'match:bg:yellow' --ignore-case --pretty --context 6 '$1' || rg --ignore-case --pretty --context 6 '$1' {}" --preview-window=up:50% --multi --select-1 --exit-0
}
# Yazi, change dir
function y() {
    local tmp="$(mktemp -t "yazi-cwd.XXXXXX")"
    yazi "$@" --cwd-file="$tmp"
    if cwd="$(command cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
        builtin cd -- "$cwd"
    fi
    rm -f -- "$tmp"
}
# /Functions }}}


# Start cargo, Rust
[[ -r "$HOME/.local/share/cargo/env" ]] && source "$HOME/.local/share/cargo/env"


# Start zoxide, a better cd
eval "$(zoxide init zsh)"

# start the better starship.rs prompt

export STARSHIP_CONFIG="$HOME/.config/starship/starship.toml"
eval "$(starship init zsh)"


if [[ ! -n $TMUX  ]]; then
    session_ids="$(tmux list-sessions 2> /dev/null)"
    if [[ -z "$session_ids" ]]; then
        tmux new-session > /dev/null
    fi
    create_new_session="Create new session"
    start_without_tmux="Start without tmux"
    choices="$session_ids\n${create_new_session}:\n${start_without_tmux}:"
    choice="$(echo $choices | fzf | cut -d: -f1)"

    if [[ "$choice" = "${create_new_session}" ]]; then
        # Create new session
        tmux new-session -A -s main > /dev/null
    elif [[ "$choice" = "${start_without_tmux}" ]]; then
        # Start without tmux
        :
    else
        # Attach existing session
        #tmux attach-session -t "$choice"
        tmux new-session -A -s "$choice" > /dev/null
    fi
fi
