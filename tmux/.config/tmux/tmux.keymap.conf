set -g prefix ^b

bind x confirm-before -p "kill-window #W? (y/n)" kill-window
bind C-x confirm-before -p "kill-session #S? (y/n)" kill-session
bind q confirm-before -p "kill-pane #P? (y/n)" kill-pane
bind g display-message
bind P paste-buffer
bind H previous-window
bind L next-window
bind r command-prompt "rename-window %%"
bind R source-file ~/.config/tmux/tmux.conf \; display "Tmux config reloaded"
bind | split-window
bind s split-window -v -c "#{pane_current_path}"
bind v split-window -h -c "#{pane_current_path}"
bind '"' choose-window
bind S choose-session
bind C-g choose-session
bind '#' select-pane -l
#bind a confirm-before -p "new session? (y/n)" new-session
bind a command-prompt -p "(new-seesion) " "new-session -A -s '%%'"
bind C-a command-prompt "rename-session %%"
bind z resize-pane -Z
bind q kill-server

bind -r H resize-pane -L 5
bind -r L resize-pane -R 5
bind -r K resize-pane -U 3
bind -r J resize-pane -D 3






version_pat='s/^tmux[^0-9]*([.0-9]+).*/\1/p'

is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"

is_fzf="ps -o state= -o comm= -t '#{pane_tty}' \
  | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?fzf$'"
bind-key -n C-h if-shell "$is_vim" "send-keys C-h" "select-pane -L"
bind-key -n C-j if-shell "($is_vim || $is_fzf)" "send-keys C-j" "select-pane -D"
bind-key -n C-k if-shell "($is_vim || $is_fzf)" "send-keys C-k" "select-pane -U"
bind-key -n C-l if-shell "$is_vim" "send-keys C-l" "select-pane -R"

tmux_version="$(tmux -V | sed -En "$version_pat")"
setenv -g tmux_version "$tmux_version"

if-shell -b '[ "$(echo "$tmux_version < 3.0" | bc)" = 1 ]' \
  "bind-key -n 'C-\\' if-shell \"$is_vim\" 'send-keys C-\\'  'select-pane -l'"
if-shell -b '[ "$(echo "$tmux_version >= 3.0" | bc)" = 1 ]' \
  "bind-key -n 'C-\\' if-shell \"$is_vim\" 'send-keys C-\\\\'  'select-pane -l'"

bind-key -T copy-mode-vi C-h select-pane -L
bind-key -T copy-mode-vi C-j select-pane -D
bind-key -T copy-mode-vi C-k select-pane -U
bind-key -T copy-mode-vi C-l select-pane -R
bind-key -T copy-mode-vi C-\\ select-pane -l
bind -T copy-mode-vi v send-keys -X begin-selection
bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'xclip -in -selection clipboard'
bind -T copy-mode-vi r send-keys -X rectangle-toggle

