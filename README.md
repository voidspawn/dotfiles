# dotfiles

## install

to install run this one liner, after you read the code of course.

```sh
curl --proto '=https' --tlsv1.2 -sSf https://gitlab.com/voidspawn/dotfiles/-/raw/main/install.sh | sh
```
