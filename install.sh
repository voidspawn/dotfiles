#!/bin/sh

# update packages, just in case.
sudo pacman -Syu

# install yay - AUR helper
sudo pacman -S --needed git base-devel && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si && yay -S yay

# adwaita-qt5 adwaita-qt6 amd-ucode autoconf automake binutils debugedit gst-plugin-pipewire gawk

# install packages

# base
yay -S --needed base base-devel git ttf-jetbrains-mono-nerd

# common
yay -S --needed dunst flatpak grub-customizer ibus network-manager-applet ntfs-3g pavucontrol udiskie

# xorg
yay -S --needed arandr dmenu feh flameshot i3-wm i3lock nitrogen picom polybar xautolock xdo xdotool xorg-xkill sxhkd screenkey

# waybar
yay -S --needed grim slurp wl-clipboard wofi

# neovim
yay -S --needed aspell-da aspell-en code codespell dotnet-host dotnet-runtime dotnet-sdk editorconfig-checker tree-sitter

# dev tools
yay -S --needed dbeaver deno git git-delta npm

# tools
yay -S --needed ripgrep bat dust figlet lolcat fd exa fish fzf gcc gzip jgmenu jq luarocks stow

# tui programs
yay -S --needed lf htop neofetch tldr wget cmatrix

# gui programs
yay -S --needed ark xpdf discord firefox gimp inkscape kitty krita libreoffice-fresh lxappearance meld gparted obsidian pcmanfm pinta qutebrowser thunderbird vlc

# config
yay -S --needed bluedevil blueman zsa-wally

# av
yay -S --needed clamav clamtk

# tty


# Enable Multilib for 32- bit support
# /etc/pacman.conf
# [multilib]
# Include = /etc/pacman.d/mirrorlist

# gaming
yay -S --needed linux-zen gameconqueror gamemode gamescope lutris steam lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader


# lib32-giflib lib32-gnutls lib32-gst-plugins-base-libs lib32-gtk3 lib32-libpulse lib32-libva lib32-libxcomposite lib32-libxinerama lib32-libxslt lib32-mpg123 lib32-ocl-icd lib32-openal lib32-v4l-utils lib32-vulkan-radeon libpulse libtool libva-mesa-driver xf86-video-amdgpu xf86-video-ati patch perl-file-mimeinfo pipewire pipewire-alsa pipewire-jack pipewire-pulse pkgconf plymouth-kcm vulkan-radeon
# clipmenu kdeconnect lxsession m4 make man-db mlocate mpd mpv qt5ct qt6ct rofi rofi-calc rofi-emoji rsync vim vim-spell-da virtualbox virtualbox-host-dkms which wine-staging wireless_tools wireplumber


# ark
# aspell-da
# aspell-en
# base
# base-devel
# bat
# bluedevil
# blueman
# breeze
# breeze-grub
# breeze-gtk
# breeze-icons
# code
# codespell
# deno
# dotnet-host
# dotnet-runtime
# dotnet-sdk
# dunst
# dust
# editorconfig-checker
# efibootmgr
# exa
# fd
# figlet
# firefox
# fish
# fzf
# gameconqueror
# gamemode
# gamescope
# gimp
# git
# git-delta
# gparted
# grim
# grub
# grub-customizer
# gtk4
# htop
# hyprland
# jgmenu
# jq
# kitty
# krita
# kvantum
# lib32-mesa
# lib32-vulkan-icd-loader
# lib32-vulkan-radeon
# libpulse
# libreoffice-fresh
# linux
# linux-firmware
# linux-lts
# linux-zen
# lolcat
# luarocks
# lutris
# lxappearance
# lxqt-openssh-askpass
# meld
# neofetch
# network-manager-applet
# networkmanager
# nodejs
# noto-fonts-emoji
# npm
# ntfs-3g
# ntp
# openssh
# pavucontrol
# pcmanfm
# pinta
# polkit-kde-agent
# qt5-wayland
# qt5ct
# qt6-wayland
# qutebrowser
# rofi
# scanmem
# sddm
# slurp
# steam
# stow
# tidy
# tldr
# tree-sitter
# tree-sitter-cli
# ttf-font-awesome
# ttf-jetbrains-mono-nerd
# ttf-roboto-mono-nerd
# udiskie
# unzip
# vlc
# vulkan-radeon
# waybar
# wget
# wireplumber
# wl-clipboard
# wofi
# xdg-desktop-portal-hyprland
# xdg-utils
# xpdf


yay --noconfirm -S wine giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls \
mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error \
lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo \
sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama \
ncurses lib32-ncurses ocl-icd lib32-ocl-icd libxslt lib32-libxslt libva lib32-libva gtk3 \
lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader



# install dotfiles
git clone https://gitlab.com/voidspawn/dotfiles.git && mv dotfiles .dotfiles && cd .dotfiles && stow /*

# install ssh demon
systemctl --user enable --now ssh-agent.service

# install rustup
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh




yay -S autojump base-devel bat bluetooth-autoconnect brave-bin cmake
yay -S composer cur dropbox dunst exa fd fish freshrss galendae-git gameconqueror
yay -S gamemode gamescope hyprland-git kvantum kvantum-qt5 lf luarocks
yay -S lxqt-config lxqt-openssh-askpass meld mpv network-manager-applet newsboat
yay -S newsflash ninja npm obs-studio openssh-askpass pavucontrol picom pnmixer
yay -S podbit qt5ct qt6ct rofi ssh-ask stow sxhkd tidy tree-sitter pynvim
yay -S ttf-icomoon-feather ttf-jetbrains-mono-nerd unzip w3m waybar
yay -S waybar-module-pacman-updates-git wbg wget wl-clipboard wlr-randr xclip
yay -S xdg-desktop-portal-hyprland-git xdg-ninja xorg-xhos yt-dlp



npm config set prefix=${XDG_DATA_HOME}/npm cache=${XDG_CACHE_HOME}/npm init-module=${XDG_CONFIG_HOME}/npm/config/npm-init.js
npm i -g eslint jsonlint tsc stylelint neovim lua-fmt prettier @quilicicf/markdown-formatter stylelint eslint




# arandr
# ark
# aspell-da
# aspell-en
# base
# base-devel
# bat
# bluedevil
# blueman
# bluez-utils
# breeze
# breeze-grub
# breeze-gtk
# breeze-icons
# code
# codespell
# deno
# dmenu
# dotnet-host
# dotnet-runtime
# dotnet-sdk
# dunst
# dust
# editorconfig-checker
# eza
# fd
# feh
# figlet
# firefox
# fish
# flameshot
# fzf
# gameconqueror
# gamemode
# gamescope
# gimp
# git
# git-delta
# gnome-themes-extra
# gparted
# grim
# gtk4
# htop
# hyprland
# i3-wm
# i3lock
# iwd
# jgmenu
# jq
# kitty
# krita
# kvantum
# lolcat
# luarocks
# lutris
# lxappearance
# lxqt-openssh-askpass
# man-db
# meld
# mlocate
# nano
# neofetch
# network-manager-applet
# networkmanager
# nitrogen
# nodejs
# noto-fonts-emoji
# npm
# ntfs-3g
# ntp
# openssh
# pavucontrol
# pcmanfm
# picom
# pinta
# polkit-kde-agent
# polybar
# powerdevil
# qt5-wayland
# qt5ct
# qt6-wayland
# qutebrowser
# rofi
# sddm
# slurp
# speech-dispatcher
# steam
# stow
# sxhkd
# systemsettings
# thunderbird
# tidy
# tldr
# tmux
# tree-sitter
# tree-sitter-cli
# ttf-font-awesome
# ttf-jetbrains-mono-nerd
# ttf-roboto-mono-nerd
# udiskie
# unrar
# unzip
# vlc
# waybar
# wezterm
# wget
# wireplumber
# wl-clipboard
# wofi
# xdg-desktop-portal-hyprland
# xdg-utils
# xdo
# xdotool
# xorg-xkill
# xpdf
# xsel
# zellij
# zram-generator
# zsa-wally-cli
