#!/bin/sh
killall polybar
if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    MONITOR=$m polybar --reload top --config=~/.config/polybar/i3config &
  done
else
  polybar --reload top --config=~/.config/polybar/i3config &
fi

