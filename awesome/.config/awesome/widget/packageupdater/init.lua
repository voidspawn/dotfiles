-------------------------------------------------
-- packageupdater Widget for Awesome Window Manager
-- Shows if packages is ready

-- @author Pavel Makhov
-- @copyright 2017 Pavel Makhov
-------------------------------------------------

local awful = require("awful")
local watch = require("awful.widget.watch")
local wibox = require("wibox")
local gears = require("gears")
local dpi = require("beautiful").xresources.apply_dpi

local PATH_TO_ICONS = gears.filesystem.get_configuration_dir() .. "widget/packageupdater/icons/"
local updateAvailable = false
local numOfUpdatesAvailable

local widget =
  wibox.widget {
  {
    id = "icon",
    widget = wibox.widget.imagebox,
    resize = true
  },
  layout = wibox.layout.align.horizontal
}

local widget_button = wibox.container.margin(widget, dpi(14), dpi(14), dpi(4), dpi(4))
widget_button:buttons(
  gears.table.join(
    awful.button(
      {},
      1,
      nil,
      function()
        if updateAvailable then
          awful.spawn("pamac-manager --updates")
        else
          awful.spawn("pamac-manager")
        end
      end
    )
  )
)
-- show a tooltip on hover
awful.tooltip(
  {
    objects = {widget_button},
    mode = "outside",
    align = "right",
    timer_function = function()
      if updateAvailable then
        return numOfUpdatesAvailable .. " updates are available"
      else
        return "We are up-to-date!"
      end
    end,
    preferred_positions = {"right", "left", "top", "bottom"}
  }
)

-- each 60 secs update the update widget
watch(
  "pamac checkupdates",
  60,
  function(_, stdout)
    numOfUpdatesAvailable = tonumber(stdout:match(".-\n"):match("%d*"))
    local widgetIconName
    if (numOfUpdatesAvailable ~= nil) then
      updateAvailable = true
      widgetIconName = "package-up"
      -- widget.visible = true
      widget_button.visible = true
    else
      updateAvailable = false
      widgetIconName = "package"
      -- widget.visible = false
      widget_button.visible = false
    end
    widget.icon:set_image(PATH_TO_ICONS .. widgetIconName .. ".svg")
    collectgarbage("collect")
  end,
  widget
)

return widget_button
