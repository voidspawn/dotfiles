#!/usr/bin/env bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}


# transparent terminal
launch_picom.sh

# background images
# feh --bg-scale --no-fehbg ~/.config/bg.jpg


# map caps to esc
setxkbmap -option caps:escape &


# polkit
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1&

# power on bluetooth
bluetoothctl power on
bluetooth-autoconnect -d &
pidof -o %PPID -x blueman-applet >/dev/null || (sleep 2s && blueman-applet &)

kdeconnect-indicator &
pidof -o %PPID -x blueman-applet >/dev/null || (nextcloud --background&)


# mount applet
pidof -o %PPID -x udiskie >/dev/null || (sleep 2s && udiskie --tray &)

# Sound applet
pidof -o %PPID -x pnmixer >/dev/null || (sleep 2s && pnmixer &)
pidof -o %PPID -x nm-applet >/dev/null || (sleep 2s && nm-applet --indicator &)


# hotkeys
pidof -o %PPID -x sxhkd >/dev/null || (sxhkd -c ~/.config/awesome/sxhkdrc &)

pidof -o %PPID -x dropbox >/dev/null || (sleep 5s && dropbox &)

sleep 1s && forcemon &
