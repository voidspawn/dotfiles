local awful = require("awful")

return {
  -- super + j
  focus_next_window = function()
    awful.client.focus.byidx(1)
  end,
  -- super + k
  focus_prev_window = function()
    awful.client.focus.byidx(-1)
  end,
  -- super + l
  focus_next_screen = function()
    awful.screen.focus_relative(1)
  end,
  -- super + h
  focus_prev_screen = function()
    awful.screen.focus_relative(-1)
  end,
  -- super + ctrl + j
  swap_next_window = function()
    awful.client.swap.byidx(1)
  end,
  -- super + ctrl + k
  swap_prev_window = function()
    awful.client.swap.byidx(-1)
  end,
  -- super + ctrl + l
  increase_master = function()
    awful.tag.incmwfact(0.05)
  end,
  -- super + ctrl + h
  decrease_master = function()
    awful.tag.incmwfact(-0.05)
  end,
  -- super + Tab
  go_back = function()
    awful.client.focus.history.previous()
    if client.focus then
      client.focus:raise()
    end
  end,
  -- super + ctrl + r
  restart_awesome = function()
    awesome.restart()
  end,
  -- super + shift + q
  quit_awesome = function()
    awesome.quit()
  end,
  -- super + e
  next_layout = function()
    awful.layout.inc(1)
  end,
  -- super + shift + e
  prev_layout = function()
    awful.layout.inc(-1)
  end,
  -- super + f
  toggle_fullscreen = function()
    client.focus.fullscreen = not client.focus.fullscreen
    client.focus:raise()
  end,
  -- super + q;q
  close_window = function()
    client.focus:kill()
  end,
  -- super + n
  toggle_floating = function()
    awful.client.floating.toggle()
  end,
  -- super + ctrl - Return
  make_master = function()
    client.focus:swap(awful.client.getmaster())
  end,
  -- super + o
  swap_screen = function()
    client.focus:move_to_screen()
  end,
  -- super + t
  toggle_ontop = function()
    client.focus.ontop = not client.focus.ontop
  end,
  -- super + m
  toggle_maximized = function()
    client.focus.maximized = not client.focus.maximized
    client.focus:raise()
  end,
  -- no binding
  toggle_minimized = function()
    client.focus.minimized = not client.focus.minimized
    client.focus:raise()
  end,
  -- super + {1-9}
  view_tag = function(i)
    local screen = awful.screen.focused()
    local tag = screen.tags[i]
    if tag then
      tag:view_only()
    end
  end,
  -- super + ctrl + {1-9}
  toggle_tag = function(i)
    local screen = awful.screen.focused()
    local tag = screen.tags[i]
    if tag then
      awful.tag.viewtoggle(tag)
    end
  end,
  -- super + shift + {1-9}
  move_to_tag = function(i)
    if client.focus then
      local tag = client.focus.screen.tags[i]
      if tag then
        client.focus:move_to_tag(tag)
      end
    end
  end,
  -- super + ctrl + shift + {1-9}
  add_to_tag = function(i)
    if client.focus then
      local tag = client.focus.screen.tags[i]
      if tag then
        client.focus:toggle_tag(tag)
      end
    end
  end,
  -- super + colon
  toggle_systray = function()
    awful.screen.focused().systray.visible = not awful.screen.focused().systray.visible
  end
}
