function scandir(directory)
  local i, t, popen = 0, {}, io.popen
  local pfile = popen('ls -a "' .. directory .. '"')
  for filename in pfile:lines() do
    i = i + 1
    t[i] = filename
  end
  pfile:close()
  return t
end
function file_exists(file)
  local f = io.open(file, "rb")
  if f then
    f:close()
  end
  return f ~= nil
end

function is_dir(path)
  local f = io.open(path, "r")
  if f == nil then
    return nil
  end
  local ok, err, code = f:read(1)
  f:close()
  return code == 21
end
function file_info(path)
  local f = io.open(path, "r")
  if f == nil then
    return nil
  end
  local ok, err, code = f:read(1)
  f:close()
  if code == 21 then
    return "dir"
  end
  return "file"
end
function lines_from(file)
  if file_info(file) ~= "file" then
    return {}
  end
  local lines = {}
  for line in io.lines(file) do
    lines[#lines + 1] = line
  end
  return lines
end
function readAll(file)
  local f = io.open(file, "rb")
  if f == nil then
    return ""
  end
  local content = f:read("*all")
  f:close()
  return content
end

function serializeTable(val, name, skipnewlines, depth)
  skipnewlines = skipnewlines or false
  depth = depth or 0

  local tmp = string.rep(" ", depth)

  if name then
    tmp = tmp .. name .. " = "
  end

  if type(val) == "table" then
    tmp = tmp .. "{" .. (not skipnewlines and "\n" or "")

    for k, v in pairs(val) do
      tmp = tmp .. serializeTable(v, k, skipnewlines, depth + 1) .. "," .. (not skipnewlines and "\n" or "")
    end

    tmp = tmp .. string.rep(" ", depth) .. "}"
  elseif type(val) == "number" then
    tmp = tmp .. tostring(val)
  elseif type(val) == "string" then
    tmp = tmp .. string.format("%q", val)
  elseif type(val) == "boolean" then
    tmp = tmp .. (val and "true" or "false")
  else
    tmp = tmp .. '"[inserializeable datatype:' .. type(val) .. ']"'
  end

  return tmp
end

function str_split(inputstr, sep)
  if type(inputstr) == "string" then
    return {}
  end
  if sep == nil then
    sep = "%s"
  end
  local t = {}
  for str in string.gmatch(inputstr, "([^" .. sep .. "]+)") do
    table.insert(t, str)
  end
  return t
end
function spliteq(str)
  t = {}
  for k, v in string.gmatch(str, "(%w+)=(.*)") do
    t["key"] = k
    t["value"] = v
  end
  return t
end
function splitcm(str)
  t = {}
  for s in string.gmatch(str, "(.-);") do
    table.insert(t, s)
  end
  return t
end

function indexOf(array, value)
  for i, v in ipairs(array) do
    if v == value then
      return i
    end
  end
  return nil
end
function keyOf(tbl, value)
  for k, v in pairs(tbl) do
    if v == value then
      return k
    end
  end
  return nil
end
function get_apps(path)
  local files = scandir(path)
  local list = {}
  for key, file in pairs(files) do
    if file_info(path .. file) == "file" then
      local lines = lines_from(path .. file)
      local name = ""
      local exec = ""
      local cats = {}
      for k, line in pairs(lines) do
        if type(line) == "string" then
          local part = spliteq(line)
          if part.key == "Name" then
            name = part.value
          elseif part.key == "Exec" then
            exec = part.value
          elseif part.key == "Categories" then
            cats = splitcm(part.value)
          end
        end
      end

      for k, cat in pairs(cats) do
        list[cat] = list[cat] or {}
        table.insert(list[cat], {name, exec})
      end
    end
  end
  table.sort(list)
  return list
end
function tableMerge(t1, t2)
  for k, v in pairs(t2) do
    if type(v) == "table" then
      if type(t1[k] or false) == "table" then
        tableMerge(t1[k] or {}, t2[k] or {})
      else
        t1[k] = v
      end
    else
      t1[k] = v
    end
  end
  return t1
end

local l1 = get_apps("/usr/share/applications/")
local l2 = get_apps("~/.local/share/applications/")
local list = {}
for key, value in pairs(tableMerge(l1, l2)) do
  local sublist = {}
  for k, val in pairs(value) do
    table.insert(sublist, val)
  end
  table.insert(list, {key, sublist})
end
