vim.cmd([[autocmd FileType json syntax match Comment +\/\/.\+$+]])
vim.opt_local.tabstop = 2
vim.opt_local.shiftwidth = 2
