vim.opt_local.autoindent = true
vim.opt_local.expandtab = true
vim.opt_local.cursorcolumn = true
vim.opt_local.cursorline = true
vim.opt_local.tabstop = 2
vim.opt_local.shiftwidth = 2
