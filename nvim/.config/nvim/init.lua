require("utils")
Import("settings")
Import("keymaps")
Import("functions")
Import("autocmds")
-- :options

ImportSetup(
  "packages",
  {
    spec = {
      -- which-key {{{
      {
        "folke/which-key.nvim",
        dependencies = {
          {"echasnovski/mini.icons", version = false, opt = true}
        },
        event = "VeryLazy",
        opts = {
          spec = {
            {"<leader>a", group = "actions", icon = {icon = "", color = "azure"}},
            {"<leader>b", group = "buffers", icon = {icon = "", color = "cyan"}},
            {"<leader>c", group = "code", icon = {icon = "", color = "orange"}},
            {"<leader>d", group = "debug", icon = {icon = "", color = "red"}},
            {"<leader>f", group = "find", icon = {icon = "󰮗", color = "green"}},
            {"<leader>g", group = "git", icon = {icon = "", color = "orange"}},
            {"<leader>h", group = "highlight", icon = {icon = "", color = "purple"}},
            {"<leader>m", group = "marks", icon = {icon = "", color = "green"}},
            {"<leader>o", group = "open", icon = {icon = "", color = "grey"}},
            {"<leader>q", group = "qlist", icon = {icon = "", color = "cyan"}},
            {"<leader>r", group = "run", icon = {icon = "󰜎", color = "red"}},
            {"<leader>s", group = "spelling", icon = {icon = "󰓆", color = "yellow"}},
            {"<leader>v", group = "vim", icon = {icon = "", color = "green"}},
            {"<leader>w", group = "window", icon = {icon = "", color = "blue"}},
            {"<leader><space>", group = "space", icon = {icon = "󱁐", color = "purple"}}
          }
        },
        keys = {
          {
            "<leader>?",
            function()
              Import("which-key").show({global = false})
            end,
            desc = "Buffer Local Keymaps (which-key)"
          }
        }
      },
      -- which-key }}}

      -- splitjoin {{{
      {
        "echasnovski/mini.splitjoin",
        version = "*",
        opts = {mapping = {toggle = ""}}
      },
      -- splitjoin }}}

      -- indentscope {{{
      {
        "echasnovski/mini.indentscope",
        version = "*",
        enabled = false,
        opts = {
          symbol = "│",
          mapping = {object_scope = "", object_scope_with_border = "", goto_top = "", goto_bottom = ""}
        }
      },
      -- indentscope }}}

      -- snacks {{{
      {
        "folke/snacks.nvim",
        priority = 1000,
        enabled = false,
        lazy = false,
        ---@type snacks.Config
        opts = {
          bigfile = {enabled = true},
          -- dashboard = {enabled = true},
          gitbrowse = {enabled = true},
          indent = {enabled = true},
          input = {enabled = true},
          notifier = {
            enabled = true,
            timeout = 3000
          },
          -- picker = {enabled = true},
          quickfile = {enabled = true},
          -- scroll = {enabled = true},
          statuscolumn = {enabled = true},
          words = {enabled = true},
          styles = {
            notification = {}
          }
        },
        keys = {
          {
            "<leader>n",
            function()
              Snacks.notifier.show_history()
            end,
            desc = "Notification History"
          },
          {
            "<leader>bd",
            function()
              Snacks.bufdelete()
            end,
            desc = "Delete Buffer"
          },
          {
            "<leader>cR",
            function()
              Snacks.rename.rename_file()
            end,
            desc = "Rename File"
          },
          {
            "<leader>gB",
            function()
              Snacks.gitbrowse()
            end,
            desc = "Git Browse",
            mode = {"n", "v"}
          },
          {
            "<leader>gb",
            function()
              Snacks.git.blame_line()
            end,
            desc = "Git Blame Line"
          },
          {
            "<leader>un",
            function()
              Snacks.notifier.hide()
            end,
            desc = "Dismiss All Notifications"
          },
          {
            "]]",
            function()
              Snacks.words.jump(vim.v.count1)
            end,
            desc = "Next Reference",
            mode = {"n", "t"}
          },
          {
            "[[",
            function()
              Snacks.words.jump(-vim.v.count1)
            end,
            desc = "Prev Reference",
            mode = {"n", "t"}
          },
          {
            "<leader>N",
            desc = "Neovim News",
            function()
              Snacks.win(
                {
                  file = vim.api.nvim_get_runtime_file("doc/news.txt", false)[1],
                  width = 0.6,
                  height = 0.6,
                  wo = {
                    spell = false,
                    wrap = false,
                    signcolumn = "yes",
                    statuscolumn = " ",
                    conceallevel = 3
                  }
                }
              )
            end
          }
        },
        init = function()
          vim.api.nvim_create_autocmd(
            "User",
            {
              pattern = "VeryLazy",
              callback = function()
                -- Setup some globals for debugging (lazy-loaded)
                _G.dd = function(...)
                  Snacks.debug.inspect(...)
                end
                _G.bt = function()
                  Snacks.debug.backtrace()
                end
                vim.print = _G.dd -- Override print to use snacks for `:=` command

                -- Create some toggle mappings
                Snacks.toggle.option("spell", {name = "Spelling"}):map("<leader>us")
                Snacks.toggle.option("wrap", {name = "Wrap"}):map("<leader>uw")
                Snacks.toggle.option("relativenumber", {name = "Relative Number"}):map("<leader>uL")
                Snacks.toggle.diagnostics():map("<leader>ud")
                Snacks.toggle.line_number():map("<leader>ul")
                Snacks.toggle.option("conceallevel", {off = 0, on = vim.o.conceallevel > 0 and vim.o.conceallevel or 2}):map(
                  "<leader>uc"
                )
                Snacks.toggle.treesitter():map("<leader>uT")
                Snacks.toggle.inlay_hints():map("<leader>uh")
                Snacks.toggle.indent():map("<leader>ug")
                Snacks.toggle.dim():map("<leader>uD")
                vim.api.nvim_create_autocmd(
                  "User",
                  {
                    pattern = "MiniFilesActionRename",
                    callback = function(event)
                      Snacks.rename.on_rename_file(event.data.from, event.data.to)
                    end
                  }
                )
              end
            }
          )
        end
      },
      -- snacks }}}

      -- notify {{{
      {
        "echasnovski/mini.notify",
        version = "*",
        config = function()
          ImportSetup(
            "mini.notify",
            {
              lsp_progress = {enable = false},
              window = {
                config = function()
                  local has_statusline = vim.o.laststatus > 0
                  local pad = vim.o.cmdheight + (has_statusline and 1 or 0)
                  return {border = "none", anchor = "SE", col = vim.o.columns, row = vim.o.lines - pad}
                end,
                winblend = 0
              }
            }
          )
          vim.notify = Import("mini.notify").make_notify()
        end
      },
      -- notify }}}

      -- searching {{{
      {
        "echasnovski/mini.files",
        version = "*",
        opts = {
          mappings = {
            close = "q",
            go_in = "l",
            go_in_plus = "<CR>",
            go_out = "h",
            go_out_plus = "H",
            mark_goto = "'",
            mark_set = "m",
            reset = "<BS>",
            reveal_cwd = "@",
            show_help = "g?",
            synchronize = "=",
            trim_left = "<",
            trim_right = ">"
          }
        }
      },
      {
        "stevearc/oil.nvim",
        priority = 100,
        dependencies = {"echasnovski/mini.icons", version = false, opt = true},
        config = function()
          ImportSetup(
            "oil",
            {
              keymaps = {
                ["g?"] = "actions.show_help",
                ["<CR>"] = "actions.select",
                -- ["<C-h>"] = false,
                ["<C-s>"] = "actions.select_split",
                ["<C-v>"] = "actions.select_vsplit",
                ["<C-t>"] = "actions.select_tab",
                ["<C-l>"] = "actions.preview",
                ["<C-c>"] = false,
                ["q"] = "actions.close",
                -- ["<C-l>"] = false,
                ["<C-e>"] = "actions.refresh",
                ["<C-h>"] = "actions.parent",
                ["<C-j>"] = "gj",
                ["<C-k>"] = "gk",
                ["-"] = "actions.parent",
                ["_"] = "actions.open_cwd",
                ["`"] = "actions.cd",
                ["~"] = "actions.tcd",
                ["gs"] = "actions.change_sort",
                ["gx"] = "actions.open_external",
                ["g."] = "actions.toggle_hidden",
                ["g\\"] = "actions.toggle_trash"
              },
              default_file_explorer = true,
              use_default_keymaps = true,
              delete_to_trash = true,
              skip_confirm_for_simple_edits = true,
              view_options = {
                show_hidden = true,
                natural_order = true,
                is_always_hidden = function(name, _)
                  return name == ".." or name == ".git"
                end
              }
            }
          )
          vim.keymap.set("n", "<leader>eo", "<CMD>Oil<CR>", {desc = "Open parent directory"})
        end
      },
      {
        "simonmclean/triptych.nvim",
        event = "VeryLazy",
        dependencies = {
          "nvim-lua/plenary.nvim", -- required
          "nvim-tree/nvim-web-devicons", -- optional for icons
          "antosha417/nvim-lsp-file-operations" -- optional LSP integration
        },
        opts = {
          mappings = {
            -- Everything below is buffer-local, meaning it will only apply to Triptych windows
            show_help = "g?",
            jump_to_cwd = ".", -- Pressing again will toggle back
            nav_left = "h",
            nav_right = {"l", "<CR>"}, -- If target is a file, opens the file in-place
            open_hsplit = {"-"},
            open_vsplit = {"|"},
            open_tab = {"<C-t>"},
            cd = "<leader>cd",
            delete = "d",
            add = "a",
            copy = "c",
            rename = "r",
            cut = "x",
            paste = "p",
            quit = "q",
            toggle_hidden = "<leader>.",
            toggle_collapse_dirs = "z"
          },
          extension_mappings = {
            ["<c-f>"] = {
              mode = "n",
              fn = function(target, _)
                require("telescope.builtin").live_grep(
                  {
                    search_dirs = {target.path}
                  }
                )
              end
            }
          },
          options = {
            dirs_first = true,
            show_hidden = true,
            collapse_dirs = true,
            line_numbers = {
              enabled = true,
              relative = true
            },
            file_icons = {
              enabled = true,
              directory_icon = "",
              fallback_file_icon = ""
            },
            responsive_column_widths = {
              -- Keys are breakpoints, values are column widths
              -- A breakpoint means "when vim.o.columns >= x, use these column widths"
              -- Columns widths must add up to 1 after rounding to 2 decimal places
              -- Parent or child windows can be hidden by setting a width of 0
              ["0"] = {0, 0.5, 0.5},
              ["120"] = {0.2, 0.3, 0.5},
              ["200"] = {0.25, 0.25, 0.5}
            },
            highlights = {
              -- Highlight groups to use. See `:highlight` or `:h highlight`
              file_names = "NONE",
              directory_names = "NONE"
            },
            syntax_highlighting = {
              -- Applies to file previews
              enabled = true,
              debounce_ms = 100
            },
            backdrop = 100, -- Backdrop opacity. 0 is fully opaque, 100 is fully transparent (disables the feature)
            transparency = 0, -- 0 is fully opaque, 100 is fully transparent
            border = "none", -- See :h nvim_open_win for border options
            max_height = 45000,
            max_width = 22000,
            margin_x = 0, -- Space left and right
            margin_y = 0 -- Space above and below
          },
          git_signs = {
            enabled = true,
            signs = {
              -- The value can be either a string or a table.
              -- If a string, will be basic text. If a table,
              -- will be passed as the {dict} argument to vim.fn.sign_define
              -- If you want to add color, you can specify a highlight group in the table.
              add = "+",
              modify = "~",
              rename = "r",
              untracked = "?"
            }
          },
          diagnostic_signs = {
            enabled = true
          }
        }
      },
      {
        "nvim-treesitter/nvim-treesitter",
        dependencies = {
          -- nav with % on more stuff
          "andymass/vim-matchup",
          -- more text objects
          "nvim-treesitter/nvim-treesitter-textobjects",
          "nvim-treesitter/nvim-treesitter-context",
          "nvim-treesitter/nvim-treesitter-refactor",
          "danymat/neogen"
        },
        build = ":TSUpdate",
        config = function()
          ImportSetup(
            "nvim-treesitter.configs",
            {
              -- A list of parser names, or "all"
              ensure_installed = {
                "bash",
                "c_sharp",
                "css",
                "diff",
                "dockerfile",
                "dot",
                "fish",
                "git_config",
                "git_rebase",
                "gitattributes",
                "gitcommit",
                "gitignore",
                "go",
                "html",
                "java",
                "javascript",
                "jsdoc",
                "json5",
                "jsonc",
                "jsonnet",
                "lua",
                "luadoc",
                "luap",
                -- "norg",
                -- "norg_meta",
                "markdown",
                "markdown_inline",
                "php",
                "phpdoc",
                "python",
                "regex",
                "rust",
                "scss",
                "sql",
                "tsx",
                "typescript",
                "vim",
                "vimdoc",
                "vue",
                "yaml",
                "zig"
              },
              additional_vim_regex_highlighting = {"markdown"},
              auto_install = true,
              highlight = {
                enable = true, -- false will disable the whole extension
                disable = {"json"}
              },
              indent = {enable = true},
              incremental_selection = {
                enable = true,
                keymaps = {
                  init_selection = "<c-space>", -- set to `false` to disable one of the mappings
                  node_incremental = "<c-space>",
                  scope_incremental = "<c-s>",
                  node_decremental = "<c-backspace>"
                }
              },
              refactor = {
                highlight_definitions = {enable = true},
                highlight_current_scope = {enable = false}
              },
              playground = {
                enable = true,
                disable = {},
                updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
                persist_queries = false -- Whether the query persists across vim sessions
              },
              query_linter = {
                enable = true,
                use_virtual_text = true,
                lint_events = {"BufWrite", "CursorHold"}
              },
              matchup = {enable = true},
              textobjects = {
                enable = false,
                select = {
                  enable = true,
                  -- Automatically jump forward to textobj, similar to targets.vim
                  lookahead = true,
                  keymaps = {
                    -- You can use the capture groups defined in textobjects.scm
                    ["af"] = "@function.outer",
                    ["if"] = "@function.inner",
                    ["ac"] = "@class.outer",
                    ["ic"] = "@class.inner"
                  }
                },
                swap = {
                  enable = false,
                  swap_next = {
                    ["<leader>a"] = "@parameter.inner"
                  },
                  swap_previous = {
                    ["<leader>A"] = "@parameter.inner"
                  }
                },
                move = {
                  enable = false,
                  set_jumps = true, -- whether to set jumps in the jumplist
                  goto_next_start = {
                    ["<leader>-"] = "@function.outer",
                    ["<leader><leader>cs"] = "@class.outer"
                  },
                  goto_next_end = {
                    ["<leader>\\"] = "@function.outer",
                    ["<leader><leader>cS"] = "@class.outer"
                  },
                  goto_previous_start = {
                    ["<leader>_"] = "@function.outer",
                    ["<leader><leader>Cs"] = "@class.outer"
                  },
                  goto_previous_end = {
                    ["¬"] = "@function.outer",
                    ["<leader><leader>CS"] = "@class.outer"
                  }
                }
              }
            }
          )

          local map = Keymap
          -- map("n", "<F12>", ":call SynStack()<cr>", "SynStack")
          -- map("n", "<S-F12>", ":TSHighlightCapturesUnderCursor<cr>", "HL captures")
          map("n", "<F12>", ":Inspect<cr>", "Inspect under cursor")

          ImportSetup("match-up", {})

          ImportSetup(
            "treesitter-context",
            {
              enable = true, -- Enable this plugin (Can be enabled/disabled later via commands)
              max_lines = 0, -- How many lines the window should span. Values <= 0 mean no limit.
              min_window_height = 0, -- Minimum editor window height to enable context. Values <= 0 mean no limit.
              line_numbers = true,
              multiline_threshold = 20, -- Maximum number of lines to show for a single context
              trim_scope = "outer", -- Which context lines to discard if `max_lines` is exceeded. Choices: 'inner', 'outer'
              mode = "cursor", -- Line used to calculate context. Choices: 'cursor', 'topline'
              -- Separator between context and content. Should be a single character string, like '-'.
              -- When separator is set, the context will only show up when there are at least 2 lines above cursorline.
              separator = nil,
              zindex = 20, -- The Z-index of the context window
              on_attach = nil -- (fun(buf: integer): boolean) return false to disable attaching
            }
          )

          -- remove the bg from the floating window
          vim.cmd([[hi TreesitterContext guibg=NONE ctermbg=NONE]])

          ImportSetup("neogen", {enabled = true, snippet_engine = "luasnip"})
        end
      },
      -- searching }}}

      -- completion {{{
      {
        {
          "saghen/blink.cmp",
          dependencies = "rafamadriz/friendly-snippets",
          version = "v0.*",
          opts = {
            keymap = {
              ["<CR>"] = {"accept", "fallback"},
              ["<Tab>"] = {
                "select_next",
                "snippet_forward",
                "fallback"
              },
              ["<S-Tab>"] = {
                "select_prev",
                "snippet_backward",
                "fallback"
              },
              ["<C-space>"] = {"show", "show_documentation", "hide_documentation"},
              ["<C-e>"] = {"hide"},
              ["<C-u>"] = {"scroll_documentation_up", "fallback"},
              ["<C-d>"] = {"scroll_documentation_down", "fallback"}
            },
            appearance = {
              use_nvim_cmp_as_default = true,
              nerd_font_variant = "mono"
            },
            cmdline = {enabled = false},
            signature = {enabled = true}
          }
        }
      },
      -- completion }}}

      -- gif diff {{{
      {
        "NeogitOrg/neogit",
        event = "VeryLazy",
        dependencies = {"nvim-lua/plenary.nvim", "sindrets/diffview.nvim", "nvim-telescope/telescope.nvim"},
        config = true
      },
      {
        "sindrets/diffview.nvim",
        dependencies = "nvim-lua/plenary.nvim",
        opts = {}
      },
      {
        "isakbm/gitgraph.nvim",
        dependencies = {"sindrets/diffview.nvim"},
        ---@type I.GGConfig
        opts = {
          symbols = {
            merge_commit = "M",
            commit = "*"
          },
          format = {
            timestamp = "%H:%M:%S %d-%m-%Y",
            fields = {"hash", "timestamp", "author", "branch_name", "tag"}
          }
        },
        init = function()
          vim.keymap.set(
            "n",
            "<leader>gl",
            function()
              require("gitgraph").draw({}, {all = true, max_count = 5000})
            end,
            {desc = "new git graph"}
          )
        end
      },
      -- gitsigns in gutter
      {
        "lewis6991/gitsigns.nvim",
        event = "VeryLazy",
        dependencies = {"nvim-lua/plenary.nvim"},
        opts = {
          current_line_blame = true,
          current_line_blame_opts = {
            virt_text = true,
            virt_text_pos = "eol", -- 'eol' | 'overlay' | 'right_align'
            delay = 300,
            ignore_whitespace = false,
            virt_text_priority = 100
          }
        }
      },
      -- gif diff }}}

      -- diagnostics {{{

      {
        "mfussenegger/nvim-lint",
        event = "VeryLazy",
        config = function()
          local lint = Import("lint")
          if lint ~= nil then
            lint.linters_by_ft = {
              markdown = {"markdownlint", "codespell"},
              text = {"codespell"},
              -- deno
              astro = {"eslint"},
              javascript = {"eslint"},
              javascriptreact = {"eslint"},
              typescript = {"eslint"},
              typescriptreact = {"eslint"},
              json = {"jsonlint"},
              lua = {"luacheck"},
              nix = {"nix"},
              php = {"php", "phpcs"},
              python = {"pylint"},
              css = {"stylelint"},
              scss = {"stylelint"},
              less = {"stylelint"},
              yaml = {"yamllint"},
              zsh = {"zsh"}
            }

            vim.api.nvim_create_autocmd(
              "BufWritePost",
              {
                group = vim.api.nvim_create_augroup("LintAutogroup", {clear = true}),
                callback = function()
                  lint.try_lint()
                end
              }
            )
          end
        end
      },
      -- Linters
      --
      -- trivy
      -- stylelint
      -- deno
      -- cspell
      -- markdownlint
      -- codespell
      -- ansible-lint
      -- csharpier
      -- htmlhint
      -- jdtls
      -- jq
      -- jsonlint
      -- markuplint
      -- misspell
      -- omnisharp
      -- ts-standard
      -- vint
      -- write-good
      -- yamllint
      {
        "rachartier/tiny-inline-diagnostic.nvim",
        event = "VeryLazy",
        config = function()
          ImportSetup("tiny-inline-diagnostic")
        end
      },
      {
        "dmmulroy/tsc.nvim",
        opts = {
          use_trouble_qflist = false,
          use_diagnostics = true
        },
        config = true,
        keys = {
          {
            "<leader>dts",
            "<cmd>TSC<cr>",
            desc = "Start type-checking"
          },
          {
            "<leader>dte",
            "<cmd>TSCStop<cr>",
            desc = "end type-checking"
          }
        }
      },
      {
        "folke/trouble.nvim",
        event = "VeryLazy",
        keys = {
          {
            "<leader>dw",
            "<cmd>Trouble diagnostics toggle<cr>",
            desc = "Diagnostics (Trouble)"
          },
          {
            "<leader>dd",
            "<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
            desc = "Buffer Diagnostics (Trouble)"
          },
          {
            "<leader>ds",
            "<cmd>Trouble symbols toggle focus=false<cr>",
            desc = "Symbols (Trouble)"
          },
          {
            "<leader>dr",
            "<cmd>Trouble lsp toggle focus=false win.position=right<cr>",
            desc = "LSP Definitions / references / ... (Trouble)"
          },
          {
            "<leader>dl",
            "<cmd>Trouble loclist toggle<cr>",
            desc = "Location List (Trouble)"
          },
          {
            "<leader>dq",
            "<cmd>Trouble qflist toggle<cr>",
            desc = "Quickfix List (Trouble)"
          }
        },
        opts = {auto_close = true}
      },
      -- diagnostics }}}

      -- formatiing {{{
      {
        "stevearc/conform.nvim",
        event = {"LspAttach", "BufReadPost", "BufNewFile"},
        config = function()
          local function strLen(s)
            local _, count = s:gsub("\n", "\n")
            return count
          end
          Import("conform").setup(
            {
              formatters = {
                eslint = {
                  command = "eslint",
                  prepend_args = {"--fix"}
                },
                luafmt = {
                  command = "luafmt",
                  -- prepend_args = { "--indent-count", 2, "--stdin" },
                  args = {"--indent-count", 2, "--stdin", "$FILENAME"}
                },
                stylua = {
                  command = "stylua",
                  prepend_args = {"--config-path", vim.fn.expand("$HOME/.config/stylua/stylua.toml")}
                },
                rustfmt = {
                  prepend_args = {"--emit=stdout", "--edition=2021"}
                },
                prettier = {
                  args = function(self, ctx)
                    if vim.endswith(ctx.filename, ".astro") then
                      return {
                        "--stdin-filepath",
                        "$FILENAME",
                        "--plugin",
                        "prettier-plugin-astro",
                        "--plugin",
                        "prettier-plugin-tailwindcss"
                      }
                    end
                    local f = io.popen("npm list -g --depth=0 | grep prettier-plugin-tailwindcss", "r")
                    local s = f:read("*a")
                    local c = strLen(s)
                    if c > 0 then
                      return {"--stdin-filepath", "$FILENAME", "--plugin", "prettier-plugin-tailwindcss"}
                    end
                    return {"--stdin-filepath", "$FILENAME"}
                  end,
                  prepend_args = {"--prose-wrap", "always"}
                },
                mdtable = {
                  command = "markdown-table-formatter"
                }
              },
              formatters_by_ft = {
                --npm install -g stylelint
                css = {"prettier", "stylelint"},
                scss = {"prettier", "stylelint"},
                less = {"prettier", "stylelint"},
                -- lua -format -- config in .config/stylua/stylua.toml
                lua = {"stylua", "luafmt"},
                -- Conform will run multiple formatters sequentially
                go = {"goimports", "gofmt"},
                html = {"prettier"},
                -- Use a sub-list to run only the first available formatter
                javascript = {"prettier"},
                javascriptreact = {"prettier"},
                typescript = {"prettier"},
                typescriptreact = {"prettier"},
                astro = {"prettier"},
                json = {"jq"},
                jsonc = {"jq"},
                -- Conform will run multiple formatters sequentially
                python = {"isort", "black"},
                rust = {"rustfmt"},
                xml = {"xmllint"},
                -- markdown = {"codespell"},
                sh = {"shfmt"},
                md = {"mdtable"}
                -- Use the "*" filetype to run formatters on all filetypes.
                -- ["*"] = { "codespell" },
                --                "vue",
                --                "yaml",
                --                "graphql",
                --                "handlebars"

                -- Formatters
                --
                -- deno
                -- fixjson
                -- prettier
                -- stylua
              },
              format_on_save = {
                -- These options will be passed to conform.format()
                timeout_ms = 2500,
                lsp_fallback = true
              },
              format_after_save = {
                lsp_fallback = true
              },
              -- Conform will notify you when a formatter errors
              notify_on_error = true
            }
          )
        end
      },
      -- formating }}}

      -- lsp {{{
      {
        "neovim/nvim-lspconfig",
        --  event = "LspAttach",
        event = {"BufReadPre", "BufNewFile"},
        dependencies = {
          -- Schema information
          "b0o/SchemaStore.nvim"
        },
        config = function()
          local lspconfig = Import("lspconfig")
          local util = Import("lspconfig.util")

          if lspconfig ~= nil and util ~= nil then
            local root_pattern = util.root_pattern

            local function getCapabilities(snippetSupport)
              local capabilities = vim.lsp.protocol.make_client_capabilities()

              if snippetSupport ~= nil then
                capabilities.textDocument.completion.completionItem.snippetSupport = true
              end

              local blink = Import("blink.cmp")
              if blink ~= nil then
                capabilities = blink.get_lsp_capabilities(capabilities)
              end
              return capabilities
            end

            local function on_attach(client, bufnr)
              if client.server_capabilities.completionProvider then
                vim.bo[bufnr].omnifunc = "v:lua.vim.lsp.omnifunc"
              end
              if client.server_capabilities.definitionProvider then
                vim.bo[bufnr].tagfunc = "v:lua.vim.lsp.tagfunc"
              end

              vim.api.nvim_set_option_value("omnifunc", "v:lua.vim.lsp.omnifunc", {buf = bufnr})
            end

            -- define servers
            -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/configs.md
            local servers = {
              angularls = {
                enabled = true,
                opts = {}
              },
              cssls = {
                enabled = true,
                opts = {
                  capabilities = getCapabilities(true)
                }
              },
              -- html
              html = {
                enabled = true,
                opts = {
                  cmd = {"html-languageserver", "--stdio"},
                  capabilities = getCapabilities(true)
                }
              },
              -- php
              -- npm i -g intelephense
              intelephense = {
                enabled = true,
                opts = {
                  settings = {
                    intelephense = {
                      format = {
                        braces = "1tbs" --[["k&r"--]]
                      },
                      environment = {phpVersion = "7.4.0"}
                    }
                  },
                  root_dir = lspconfig.util.root_pattern(".git", "composer.json", "package.json"),
                  handlers = {
                    ["client/registerCapability"] = function(_, _, _, _)
                      return {result = nil, error = nil}
                    end,
                    ["workspace/configuration"] = function(_, _, _, _)
                      return {result = nil, error = nil}
                    end
                  }
                }
              },
              -- json
              -- npm install -g vscode-json-languageserver
              jsonls = {
                enabled = true,
                opts = {
                  cmd = {"vscode-json-languageserver", "--stdio"},
                  capabilities = getCapabilities(true),
                  commands = {
                    Format = {
                      function()
                        vim.lsp.buf.range_formatting({}, {0, 0}, {vim.fn.line("$"), 0})
                      end
                    }
                  },
                  settings = {
                    json = {
                      schemas = require("schemastore").json.schemas(),
                      validate = {enable = true}
                    }
                  }
                }
              },
              -- lua
              lua_ls = {
                enabled = true,
                opts = {
                  --cmd = {sumneko_binary, "-E", sumneko_root_path .. "/main.lua"},
                  settings = {
                    Lua = {
                      runtime = {
                        -- Tell the language server which version of Lua you're using
                        -- (most likely LuaJIT in the case of Neovim)
                        version = "LuaJIT"
                      },
                      diagnostics = {
                        -- Get the language server to recognize the `vim` global
                        globals = {"vim", "Import", "ImportSetup", "D", "os"}
                      },
                      workspace = {
                        -- Make the server aware of Neovim runtime files
                        library = {
                          -- vim.api.nvim_get_runtime_file("", true),
                          [vim.fn.expand("$VIMRUNTIME/lua")] = true,
                          [vim.fn.expand("$VIMRUNTIME/lua/vim/lsp")] = true
                        },
                        checkThirdParty = false,
                        maxPreload = 10000
                      },
                      telemetry = {
                        enable = false
                      },
                      format = {
                        enable = true,
                        defaultConfig = {
                          indent_style = "space",
                          indent_size = "2",
                          quote_style = "double",
                          call_arg_parentheses = "keep",
                          continuation_indent = "2",
                          trailing_table_separator = "never",
                          insert_final_newline = true,
                          space_around_table_field_list = true,
                          align_call_args = true,
                          align_function_params = true,
                          align_continuous_assign_statement = true,
                          align_continuous_rect_table_field = true,
                          align_array_table = true
                        }
                      },
                      hint = {
                        enable = true,
                        arrayIndex = "Auto",
                        await = true,
                        paramName = "All",
                        paramType = true,
                        semicolon = "SameLine",
                        setType = true
                      }
                    }
                  }
                }
              },
              -- astro
              astro = {
                enabled = true,
                opts = {}
              },
              -- typescript
              -- npm install -g typescript typescript-language-server
              vtsls = {
                enabled = true,
                opts = {
                  settings = {
                    typescript = {
                      preferences = {
                        importModuleSpecifier = "non-relative"
                      }
                    }
                  }
                }
              },
              -- vim
              -- npm install -g vim-language-server
              vimls = {
                enabled = true,
                opts = {}
              },
              -- tailwind (css)
              -- npm install -g @tailwindcss/language-server
              tailwindcss = {
                enabled = true,
                opts = {}
              }
            }

            -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
            vim.api.nvim_create_autocmd(
              "LspAttach",
              {
                callback = function(args)
                  local bufnr = args.buf
                  local client = vim.lsp.get_client_by_id(args.data.client_id)
                  on_attach(client, bufnr)
                end
              }
            )

            for name, server in pairs(servers) do
              if server.enabled then
                lspconfig[name].setup(server.opts)
              end
            end
          end
        end
      },
      -- lsp }}}

      -- markdown {{{
      {
        "MeanderingProgrammer/render-markdown.nvim",
        lazy = true,
        ft = "markdown",
        opts = {},
        dependencies = {"nvim-treesitter/nvim-treesitter", "nvim-tree/nvim-web-devicons"}
      },
      {
        "iamcco/markdown-preview.nvim",
        lazy = true,
        cmd = {"MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop"},
        build = "cd app && yarn install",
        init = function()
          vim.g.mkdp_filetypes = {"markdown"}
        end,
        ft = {"markdown"}
      },
      --https://github.com/toppair/peek.nvim
      {
        "jghauser/follow-md-links.nvim",
        ft = {"markdown"},
        config = function()
          --vim.keymap.set("n", "<bs>", ":edit #<cr>", {silent = true})
        end
      },
      -- markdown }}}

      -- nav {{{

      {
        "fresh2dev/zellij.vim",
        lazy = false,
        init = function()
          -- Options:
          -- vim.g.zelli_navigator_move_focus_or_tab = 1
          -- vim.g.zellij_navigator_no_default_mappings = 1
        end,
        vim.keymap.set({"n", "t"}, "<leader>mn", "<CMD>Lazy reload zellij.vim<CR>"),
        vim.keymap.set({"n", "t"}, "<C-h>", "<CMD>ZellijNavigatorLeft<CR>"),
        vim.keymap.set({"n", "t"}, "<C-l>", "<CMD>ZellijNavigatorRight<CR>"),
        vim.keymap.set({"n", "t"}, "<C-k>", "<CMD>ZellijNavigatorUp<CR>"),
        vim.keymap.set({"n", "t"}, "<C-j>", "<CMD>ZellijNavigatorDown<CR>")
      },
      {
        "folke/flash.nvim",
        event = "VeryLazy",
        ---@type Flash.Config
        opts = {},
        -- stylua: ignore
        keys = {
          {
            "s",
            mode = {"n", "x", "o"},
            function()
              require("flash").jump()
            end,
            desc = "Flash"
          },
          {
            "S",
            mode = {"n", "x", "o"},
            function()
              require("flash").treesitter()
            end,
            desc = "Flash Treesitter"
          },
          {
            "<c-s>",
            mode = {"c"},
            function()
              require("flash").toggle()
            end,
            desc = "Toggle Flash Search"
          }
        }
      },
      {"declancm/maximize.nvim", config = true},
      {"echasnovski/mini.ai", version = "*", config = true},
      {
        "echasnovski/mini.surround",
        version = "*",
        config = true,
        opts = {
          mappings = {
            add = "ys", -- Add surrounding in Normal and Visual modes
            replace = "cs", -- Replace surrounding
            delete = "ds", -- Delete surrounding
            find = "", -- Find surrounding (to the right)
            find_left = "", -- Find surrounding (to the left)
            highlight = "", -- Highlight surrounding
            update_n_lines = "", -- Update `n_lines`
            suffix_last = "",
            suffix_next = ""
          },
          search_method = "cover_or_next"
        }
      },
      {
        "declancm/maximize.nvim",
        config = true
      },
      {
        "nvim-telescope/telescope.nvim",
        event = "VeryLazy",
        dependencies = {
          "nvim-lua/popup.nvim",
          "nvim-lua/plenary.nvim",
          -- icons for telescope
          "nvim-tree/nvim-web-devicons",
          -- use telescope for vim.ui.select ex. vim.lsp.buf.code_action

          "tsakirist/telescope-lazy.nvim",
          "nvim-telescope/telescope-ui-select.nvim",
          {"nvim-telescope/telescope-fzf-native.nvim", build = "make"},
          "debugloop/telescope-undo.nvim"
        },
        config = function()
          -- telescope
          -- This will load fzy_native and have it override the default file sorter
          -- require('telescope').load_extension('fzy_native')
          local action_state = Import("telescope.actions.state")
          local actions = Import("telescope.actions")
          if actions ~= nil then
            actions.master_stack = function(prompt_bufnr)
              local picker = action_state.get_current_picker(prompt_bufnr)
              actions.close(prompt_bufnr)
              vim.cmd([[tabnew]])
              for index, entry in ipairs(picker:get_multi_selection()) do
                if index == 1 then
                  vim.cmd("edit " .. entry.filename)
                elseif index == 2 then
                  vim.cmd("vsplit " .. entry.filename)
                else
                  vim.cmd("split " .. entry.filename)
                end
              end
              vim.cmd([[wincmd =]])
            end
          end
          -- use % instead of == for a mosaic effect no?-
          local previewer = Import("telescope.previewers")
          if previewer ~= nil then
            ImportSetup(
              "telescope",
              {
                defaults = {
                  vimgrep_arguments = {
                    "rg",
                    "--color=never",
                    "--no-heading",
                    "--with-filename",
                    "--line-number",
                    "--column",
                    "--hidden",
                    "--smart-case",
                    "--trim"
                  },
                  prompt_prefix = " ",
                  selection_caret = " ",
                  entry_prefix = "  ",
                  initial_mode = "insert",
                  selection_strategy = "reset",
                  sorting_strategy = "descending",
                  layout_strategy = "horizontal",
                  layout_config = {
                    horizontal = {mirror = false},
                    vertical = {mirror = false}
                  },
                  -- file_sorter = require'telescope.sorters'.get_fzy_sorter, -- get_fuzzy_file,
                  file_ignore_patterns = {
                    "tags/.*",
                    ".git/.*",
                    "node_modules/.*",
                    "vendor/.*",
                    "local/coursebase/archive/.*",
                    "*.min.js"
                  },
                  -- generic_sorter = require'telescope.sorters'.get_fzy_sorter, -- get_generic_fuzzy_sorter,
                  winblend = 0, --15,
                  border = {},
                  borderchars = {"─", "│", "─", "│", "╭", "╮", "╯", "╰"},
                  color_devicons = true,
                  use_less = false,
                  path_display = {"filename_first"},
                  set_env = {["COLORTERM"] = "truecolor"}, -- default = nil,
                  preview = {filesize_limit = 1},
                  file_previewer = require("telescope.previewers").vim_buffer_cat.new,
                  grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,
                  qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new,
                  -- Developer configurations: Not meant for general override
                  buffer_previewer_maker = function(filepath, bufnr, opts)
                    opts = opts or {}

                    if opts.use_ft_detect == nil then
                      local ft = require("plenary.filetype").detect(filepath)
                      -- Here for example you can say: if ft == "xyz" then this_regex_highlighing else nothing end
                      opts.use_ft_detect = false
                      require("telescope.previewers.utils").regex_highlighter(bufnr, ft)
                    end

                    filepath = vim.fn.expand(filepath)
                    vim.loop.fs_stat(
                      filepath,
                      function(_, stat)
                        if not stat then
                          return
                        end
                        if stat.size > 100000 then
                          return
                        else
                          require("telescope.previewers").buffer_previewer_maker(filepath, bufnr, opts)
                        end
                      end
                    )
                  end,
                  -- buffer_previewer_maker = require'telescope.previewers'.buffer_previewer_maker,
                  mappings = {
                    i = {
                      ["<C-j>"] = actions.move_selection_next,
                      ["<C-k>"] = actions.move_selection_previous,
                      -- To disable a keymap, put [map] = false
                      -- So, to not map "<C-n>", just put

                      -- Otherwise, just set the mapping to the function that you want it to be.
                      ["<C-x>"] = false,
                      -- add a new one on s or v
                      ["<C-s>"] = actions.select_horizontal,
                      ["<m-m>"] = actions.master_stack,
                      -- Add up multiple actions
                      ["<CR>"] = actions.select_default + actions.center,
                      ["<C-q>"] = actions.send_to_qflist,
                      ["<C-h>"] = "which_key"
                    },
                    n = {
                      ["<C-j>"] = actions.move_selection_next,
                      ["<C-k>"] = actions.move_selection_previous,
                      ["<esc>"] = actions.close
                    }
                  }
                },
                pickers = {
                  find_files = {
                    theme = "ivy"
                  }
                },
                extensions = {
                  fzf = {
                    fuzzy = true, -- false will only do exact matching
                    override_generic_sorter = true, -- override the generic sorter
                    override_file_sorter = true, -- override the file sorter
                    case_mode = "smart_case" -- or "ignore_case" or "respect_case"
                    -- the default case_mode is "smart_case"
                  },
                  -- file_browser = {
                  --   theme = "ivy"
                  -- },
                  ["ui-select"] = {
                    require("telescope.themes").get_dropdown({})
                  },
                  lazy = {
                    -- Optional theme (the extension doesn't set a default theme)
                    theme = "ivy",
                    -- Whether or not to show the icon in the first column
                    show_icon = true,
                    -- Mappings for the actions
                    mappings = {
                      open_in_browser = "<C-o>",
                      open_in_file_browser = "<M-b>",
                      open_in_find_files = "<C-f>",
                      open_in_live_grep = "<C-g>",
                      open_plugins_picker = "<C-b>", -- Works only after having called first another action
                      open_lazy_root_find_files = "<C-r>f",
                      open_lazy_root_live_grep = "<C-r>g"
                    }
                  }
                }
              }
            )
          end

          local telescope = Import("telescope")
          if telescope ~= nil then
            -- require('telescope').load_extension('fzy_native')
            require("telescope").load_extension("fzf")
            --require("telescope").load_extension("file_browser")
            require("telescope").load_extension("ui-select")
            require("telescope").load_extension("lazy")
            require("telescope").load_extension("undo")
            -- -- https://github.com/lambdalisue/fern-renderer-devicons.vim
            -- -- let g:fern#renderer = "devicons"
            -- -- https://github.com/lambdalisue/fern-renderer-nerdfont.vim
            -- vim.cmd [[let g:fern#renderer = "nerdfont"]]
            --
            -- --https://github.com/lambdalisue/fern-git-status.vim
            -- --" Disable listing ignored files/directories
            -- vim.cmd [[let g:fern_git_status#disable_ignored = 1]]
            --
            -- --" Disable listing untracked files
            -- vim.cmd [[let g:fern_git_status#disable_untracked = 1]]
            --
            -- --" Disable listing status of submodules
            -- vim.cmd [[let g:fern_git_status#disable_submodules = 1]]
            --
            -- --" Disable listing status of directories
            -- vim.cmd [[let g:fern_git_status#disable_directories = 1]]

            local actions = require("telescope.actions")
            local action_state = require("telescope.actions.state")
            local pickers = require("telescope.pickers")
            local finders = require("telescope.finders")
            local previewers = require("telescope.previewers")
            local conf = require("telescope.config").values
            local tsbuiltin = require("telescope.builtin")
            local plugin_docs = function(opts)
              local plugins = {}
              for k, v in pairs(_G.packer_plugins) do
                table.insert(plugins, {k, v})
              end
              pickers.new(
                opts or {},
                {
                  prompt_title = "Plugins",
                  finder = finders.new_table(
                    {
                      results = plugins,
                      entry_maker = function(entry)
                        return {
                          value = entry,
                          display = entry[1],
                          ordinal = entry[1],
                          path = entry[2].path .. "/README.md"
                        }
                      end
                    }
                  ),
                  sorter = conf.generic_sorter(opts),
                  previewer = previewers.vim_buffer_cat:new(),
                  attach_mappings = function(prompt_bufnr, _)
                    actions.select_default:replace(
                      function()
                        actions.close(prompt_bufnr)
                        local path = action_state.get_selected_entry().path
                        --vim.cmd("vsplit")
                        vim.cmd("split")
                        local win = vim.api.nvim_get_current_win()
                        local buf = vim.api.nvim_create_buf(true, true)
                        vim.api.nvim_win_set_buf(win, buf)
                        vim.cmd("e " .. path)
                        vim.bo.modifiable = false
                        vim.bo.buflisted = false
                        vim.bo.bufhidden = "hide"
                      end
                    )
                    return true
                  end
                }
              ):find()
            end
            local silent = {silent = true}
            local mapl = Keymapl
            mapl("n", "he", ":mes<CR>", "Show vim messages")
            mapl("n", "hh", tsbuiltin.help_tags, "Search Help")
            mapl("n", "hm", tsbuiltin.man_pages, "Man Pages")
            mapl("n", "ht", tsbuiltin.builtin, "Telescope pickers")
            mapl("n", "hp", plugin_docs, "Plugin Documentation")

            mapl(
              "n",
              "ff",
              function()
                tsbuiltin.find_files(
                  {
                    search_dirs = {require("plenary.path"):new("."):absolute()},
                    hidden = true
                  }
                )
              end,
              "find files"
            )
            mapl(
              "n",
              "fi",
              function()
                tsbuiltin.git_files({show_untracked = true})
              end,
              "git files"
            )
            mapl("n", "fp", tsbuiltin.git_status, "git changed files")
            mapl("n", "fj", tsbuiltin.jumplist, "jumplist")
            mapl("n", "fk", tsbuiltin.keymaps, "keymaps")
            mapl("n", "fl", tsbuiltin.loclist, "loclist", silent)
            mapl("n", "fm", tsbuiltin.marks, "marks")
            mapl("n", "fo", tsbuiltin.oldfiles, "Open recent file")
            mapl("n", "fq", tsbuiltin.quickfix, "quickfix", silent)
            mapl("n", "fr", tsbuiltin.lsp_references, "references", silent)
            mapl(
              "n",
              "fs",
              function()
                tsbuiltin.live_grep(
                  {
                    cmd = require("plenary.path"):new("."):absolute(),
                    additional_args = {"-j1"}
                  }
                )
              end,
              "grep - live",
              silent
            )

            mapl("n", "fz", tsbuiltin.lsp_workspace_symbols, "symbols")
            --  registers
            mapl("n", 'f"', tsbuiltin.registers, "registers")
            mapl("n", "fx", tsbuiltin.spell_suggest, "spell suggest")
            mapl("n", "fd", tsbuiltin.resume, "resume last search")
            mapl(
              "n",
              "fg",
              function()
                tsbuiltin.grep_string(
                  {
                    cmd = require("plenary.path"):new("."):absolute(),
                    search = vim.fn.input("  "),
                    hidden = true
                  }
                )
              end,
              "grep - string in",
              silent
            )

            mapl(
              "n",
              "fu",
              function()
                Import("telescope").extensions.undo.undo({use_delta = true})
              end,
              "undo tree"
            )
            mapl(
              "n",
              "fw",
              function()
                tsbuiltin.grep_string(
                  {
                    cmd = require("plenary.path"):new("."):absolute(),
                    search = vim.fn.expand("<cword>"),
                    hidden = true
                  }
                )
              end,
              "grep - word",
              silent
            )
          end
        end
      },
      -- nav }}}

      -- theme ui {{{
      {
        "Bekaboo/dropbar.nvim",
        event = "VeryLazy",
        dependencies = {
          "nvim-telescope/telescope-fzf-native.nvim"
        }
      },
      {
        "nvim-lualine/lualine.nvim",
        dependencies = {
          {"nvim-tree/nvim-web-devicons", opt = true},
          "arkav/lualine-lsp-progress"
        },
        config = function()
          local recording = 0
          ImportSetup(
            "lualine",
            {
              options = {
                theme = {
                  normal = {
                    -- "#1f2335"
                    a = {bg = "#7aa2f7", fg = "#000000"},
                    b = {bg = "#3b4261", fg = "#7aa2f7"},
                    c = {bg = "NONE", fg = "#a9b1d6"}
                  },
                  insert = {
                    a = {bg = "#9ece6a", fg = "#000000"},
                    b = {bg = "#3b4261", fg = "#9ece6a"},
                    c = {bg = "NONE", fg = "#a9b1d6"}
                  },
                  command = {
                    a = {bg = "#e0af68", fg = "#000000"},
                    b = {bg = "#3b4261", fg = "#e0af68"},
                    c = {bg = "NONE", fg = "#a9b1d6"}
                  },
                  visual = {
                    a = {bg = "#bb9af7", fg = "#000000"},
                    b = {bg = "#3b4261", fg = "#bb9af7"},
                    c = {bg = "NONE", fg = "#a9b1d6"}
                  },
                  replace = {
                    a = {bg = "#f7768e", fg = "#000000"},
                    b = {bg = "#3b4261", fg = "#f7768e"},
                    c = {bg = "NONE", fg = "#a9b1d6"}
                  },
                  inactive = {
                    a = {bg = "#7aa2f7", fg = "#000000"},
                    -- a = {bg = "NONE", fg = "#7aa2f7"},
                    b = {bg = "NONE", fg = "#3b4261", gui = "bold"},
                    c = {bg = "NONE", fg = "#3b4261"}
                  }
                },
                component_separators = {left = ">", right = "<"},
                section_separators = {left = "", right = ""}
              },
              sections = {
                lualine_a = {
                  {
                    "mode",
                    fmt = function(str)
                      local res = str:sub(1, 1)
                      if str == "NORMAL" then
                        res = ""
                      elseif str == "VISUAL" or str == "V-LINE" or str == "V-BLOCK" then
                        res = ""
                      elseif str == "INSERT" or str == "REPLACE" then
                        res = ""
                      elseif str == "COMMAND" or str == "EX" then
                        res = ""
                      elseif str == "TERMINAL" then
                        res = ""
                      end

                      return res
                    end
                  }
                },
                lualine_b = {
                  "branch",
                  "diff",
                  "diagnostics",
                  function()
                    if recording == 1 then
                      return "󰑋"
                    end
                    return ""
                  end
                },
                lualine_c = {"buffers", "lsp_progress"},
                lualine_x = {"encoding", "fileformat", "filetype"},
                lualine_y = {"progress"},
                lualine_z = {"location"}
              },
              inactive_sections = {
                lualine_a = {},
                lualine_b = {},
                lualine_c = {"buffers"},
                lualine_x = {"location"},
                lualine_y = {},
                lualine_z = {}
              },
              tabline = {},
              -- winbar = {
              --   lualine_a = {},
              --   lualine_b = {},
              --   lualine_c = {},
              --   lualine_x = {{git_blame.get_current_blame_text, cond = git_blame.is_blame_text_available}},
              --   lualine_y = {},
              --   lualine_z = {}
              -- },
              -- inactive_winbar = {},
              extensions = {
                "lazy",
                "man",
                "mason",
                "neo-tree",
                "nvim-dap-ui",
                "nvim-tree",
                "quickfix",
                "toggleterm",
                "trouble"
              }
            }
          )

          -- {
          --   function()
          --     return navic.get_location()
          --   end,
          --   cond = function()
          --     return navic.is_available()
          --   end
          -- }
          -- {gps.get_location, cond = gps.is_available, color = {fg = "#4E5882"}}

          vim.api.nvim_set_hl(0, "SLSeparator", {fg = "#99A3CD", bg = "NONE"})

          vim.api.nvim_create_autocmd(
            "RecordingEnter",
            {
              callback = function()
                recording = 1
              end
            }
          )
          vim.api.nvim_create_autocmd(
            "RecordingLeave",
            {
              callback = function()
                recording = 0
              end
            }
          )
        end
      },
      -- better json support
      {"elzr/vim-json", ft = "json"},
      {
        "catgoose/nvim-colorizer.lua",
        event = "BufReadPre",
        opts = {}
      },
      {
        "echasnovski/mini.colors",
        version = "*",
        config = function()
          local MiniColors = require("mini.colors")
          MiniColors.get_colorscheme()
        end
      },
      {
        "catgoose/nvim-colorizer.lua",
        event = "BufReadPre",
        opts = {
          mode = "virtualtext",
          tailwind = true,
          sass = {enable = true},
          virtualtext_inline = true
        }
      },
      {
        "navarasu/onedark.nvim",
        priority = 1000,
        config = function()
          require("onedark").setup(
            {
              style = "deep",
              transparent = true,
              term_colors = true,
              lualine = {transparent = true}
            }
          )
          require("onedark").load()

          -- hl.TelescopeNormal = {bg = c.none}
          -- hl.TelescopeBorder = {bg = c.none, fg = c.bg_dark}
          -- hl.TelescopePromptNormal = {bg = c.none}
          -- hl.TelescopePromptBorder = {bg = c.none, fg = prompt}
          -- hl.TelescopePromptTitle = {bg = c.none, fg = prompt}
          -- hl.TelescopePreviewTitle = {bg = c.none, fg = c.bg_dark}
          -- hl.TelescopeResultsTitle = {bg = c.none, fg = c.bg_dark}
          --
          -- hl.NvimTreeNormal = {bg = c.none}
          -- hl.NvimTreeNormalNC = {bg = c.none}
          --
          -- hl.WhichKeyFloat = {bg = c.none}
          -- hl.WhichKeyBorder = {bg = c.none}
          -- hl.QuickFixLine = {bg = c.none}
          -- hl.WildMenu = {bg = c.none}
          -- hl.CursorColumn = {bg = "#0f0f0f"}
          -- hl.CursorLine = {bg = "#0f0f0f"}
          -- hl.PmenuSbar = {bg = c.none}
          -- hl.StatusLine = {bg = c.none}
          -- hl.StatusLineNC = {bg = c.none}
          -- hl.Comment = {fg = "#5db445"}
          -- hl.GitSignsCurrentLineBLame = {fg = "#5db445"}

          vim.fn.matchadd("DiagnosticInfo", "\\(TODO:\\)")
          vim.fn.matchadd("DiagnosticWarn", "\\(HACK:\\)")
          vim.fn.matchadd("DiagnosticWarn", "\\(WARN:\\)")
          vim.fn.matchadd("DiagnosticWarn", "\\(WARNING:\\)")
          vim.fn.matchadd("DiagnosticWarn", "\\(XXX:\\)")
          vim.fn.matchadd("Identifier", "\\(PERF:\\)")
          vim.fn.matchadd("Identifier", "\\(PERFORMANCE:\\)")
          vim.fn.matchadd("Identifier", "\\(OPTIM:\\)")
          vim.fn.matchadd("Identifier", "\\(OPTIMIZE:\\)")
          vim.fn.matchadd("DiagnosticHint", "\\(NOTE:\\)")
          vim.fn.matchadd("Identifier", "\\(TEST:\\)")
          vim.fn.matchadd("Identifier", "\\(TESTING:\\)")
          vim.fn.matchadd("Identifier", "\\(PASSED:\\)")
          vim.fn.matchadd("Identifier", "\\(FAILED:\\)")

          -- lualine
          vim.api.nvim_set_hl(0, "SLSeparator", {fg = "#99A3CD", bg = "NONE"})
          vim.api.nvim_set_hl(0, "StatusLine", {fg = "#99A3CD", bg = "NONE"})
          vim.api.nvim_set_hl(0, "StatusLineNC", {fg = "#99A3CD", bg = "NONE"})
          vim.api.nvim_set_hl(0, "PmenuSbar", {bg = "NONE"})
          vim.api.nvim_set_hl(0, "NormalFloat", {fg = "#93a4c3", bg = "NONE"})
          vim.api.nvim_set_hl(0, "WinBar", {link = "StatusLine"})
          vim.api.nvim_set_hl(0, "WinBarNC", {link = "StatusLineNC"})

          vim.cmd(
            [[
      highlight CursorColumn ctermbg=none guibg=#0f0f0f gui=NONE cterm=NONE blend=20
      highlight CursorLine ctermbg=none guibg=#0f0f0f gui=NONE cterm=NONE blend=20
      " highlight Normal ctermbg=none guibg=none
      " highlight NormalNC ctermbg=none guibg=none
      " highlight LineNr ctermbg=none guibg=none
      " highlight SignColumn ctermbg=none guibg=none

      augroup CursorLineTransparency
        autocmd!
        autocmd ColorScheme * highlight CursorColumn guibg=#0f0f0f ctermbg=none blend=20
        autocmd ColorScheme * highlight CursorLine guibg=#0f0f0f ctermbg=none blend=20
      augroup END
    ]]
          )
        end
      },
      {
        "aileot/ex-colors.nvim",
        lazy = true,
        cmd = "ExColors",
        ---@type ExColors.Config
        opts = {}
      },
      -- theme ui }}}

      -- system {{{

      -- auto set root
      {
        "notjedi/nvim-rooter.lua",
        event = "VeryLazy",
        opts = {}
      },
      {
        "LunarVim/bigfile.nvim",
        enabled = false
      },
      -- installer for LSP, DAP, LINT, Formatter
      {
        "WhoIsSethDaniel/mason-tool-installer.nvim",
        dependencies = {"williamboman/mason.nvim"},
        build = ":MasonUpdate",
        priority = 900,
        config = function()
          ImportSetup("mason")

          ImportSetup(
            "mason-tool-installer",
            {
              ensure_installed = {
                "tailwindcss-language-server",
                "astro-language-server",
                "angular-language-server",
                "deno",
                "css-lsp",
                "json-lsp",
                "eslint-lsp",
                "html-lsp",
                "cssmodules-language-server",
                "intelephense",
                "jdtls",
                "lua-language-server",
                "markdownlint",
                "prettier",
                "stylua",
                "typescript-language-server",
                "vim-language-server",
                "vint",
                "vtsls",
                "yaml-language-server",
                "yamllint"
              },
              auto_update = false
            }
          )
        end
      }
      -- system }}}
    }
  }
)

-- vim:foldmethod=marker:foldenable
