vim.g.mapleader = " "
vim.g.maplocalleader = ","

local expr = {expr = true}
local silent = {silent = true}

local map = Keymap
local mapl = Keymapl

map("n", "gQ", "<nop>", "disable ex-mode")
map("n", "q:", "<nop>", "disable ex-mode")

vim.keymap.set("n", "<space><space>x", "<cmd>source %<CR>")
vim.keymap.set("n", "<space>x", ":.lua<CR>")
vim.keymap.set("v", "<space>x", ":lua<CR>")

mapl("n", "rs", "!!$SHELL <cr>", "Run shell")
mapl("n", "rl", "<cmd>.lua<CR>", "Execute the current lua line")
mapl("n", "rf", "<cmd>source %<CR>", "Execute the current lua/vim file")
mapl("n", "rv", 'yy:normal @"<cr>', "Run vim script")

vim.keymap.set(
  "n",
  "gs",
  function()
    local MiniSplitjoin = Import("mini.splitjoin")
    MiniSplitjoin.toggle()
  end
)

vim.keymap.set(
  "n",
  "<leader>ee",
  function()
    local miniFiles = Import("mini.files")
    if miniFiles ~= nil then
      local _ = miniFiles.close() or miniFiles.open(vim.api.nvim_buf_get_name(0), false)
      vim.defer_fn(
        function()
          miniFiles.reveal_cwd()
        end,
        30
      )
    end
  end,
  {desc = "Open mini files"}
)
vim.keymap.set("n", "<leader>et", ":Triptych<CR>", {desc = "Open triptuch files"})
-- Noob Helping Keymappings
map("i", "<up>", "<nop>", "nop")
map("i", "<down>", "<nop>", "nop")
map("i", "<right>", "<nop>", "nop")
map("i", "<left>", "<nop>", "nop")
-- undo mark

map("i", ";", ";<c-g>u", "add undomark")
map("i", ",", ",<c-g>u", "add undomark")
map("i", ".", ".<c-g>u", "add undomark")
map("i", "!", "!<c-g>u", "add undomark")
map("i", "?", "?<c-g>u", "add undomark")
map("i", "-", "-<c-g>u", "add undomark")
map("i", "+", "+<c-g>u", "add undomark")
map("i", "/", "/<c-g>u", "add undomark")
map("i", "*", "*<c-g>u", "add undomark")
map("i", "\\", "\\<c-g>u", "add undomark")
map("i", "(", "(<c-g>u", "add undomark")
map("i", ")", ")<c-g>u", "add undomark")
map("i", ">", "><c-g>u", "add undomark")
map("i", "<", "<<c-g>u", "add undomark")
map("i", "[", "[<c-g>u", "add undomark")
map("i", "]", "]<c-g>u", "add undomark")
map("i", "{", "{<c-g>u", "add undomark")
map("i", "}", "}<c-g>u", "add undomark")
-- convert tab to spaces
mapl("n", "ct", "<cmd>retab<cr>", "convert tab to spaces")

-- diff before save
mapl("n", "db", ":w !diff % -<cr>", "diff before save")

mapl("n", "oc", "<cmd>DiffOrig<cr>", "show changes")
-- add symbol to end
mapl("n", ";", "mzA;<Esc>`z", "add ; in end")
mapl("n", ",", "mzA,<Esc>`z", "add , in end")
mapl("n", "x", "mz$x`z", "remove last char in end")

-- Reselect last-pasted text
map("n", "gv", "`[v`]", "reselect last-pasted text")

-- n - next center screen
map("n", "n", "nzzzv", "next search")
map("n", "N", "Nzzzv", "previous search")
map("n", "J", "mzJ`z", "keep cursor at start of join")
-- copy to end of line
map("n", "Y", "yg$", "yank to end of line")
-- nav

-- fix backspace
map("n", "<bs>", "X", "delete char back")

-- fix esc to clear search
map("n", "<Esc>", "<cmd>nohlsearch<cr>", "clear on esc")
-- fix enter to clear search
map(
  "n",
  "<CR>",
  function()
    if vim.opt.hlsearch:get() then
      vim.cmd.nohl()
      return "<CR>"
    else
      return "<CR>"
    end
  end,
  "clear on enter",
  expr
)

-- fix p so it don't cut text
-- make paste in virtual mode replace and keep reg 0 as is
map("v", "p", '"_dP', "paste", silent)

-- fix tab indent
map("v", "<Tab>", ">gv", "move line forward")
map("v", "<S-Tab>", "<gv", "move line back")
-- spelling
mapl("n", "sn", "ms[s1z=*`s", "next spelling")
mapl("n", "sN", "ms]s1z=*`s", "prev spelling")
mapl("n", "sl", "[sz=", "list for next spelling")

-- buffer nav
mapl("n", "oo", "<C-^>", "Open other file")

-- window nav

-- move window
map("n", "<C-S-h>", "<C-w>H", "move window left")
map("n", "<C-S-j>", "<C-w>J", "move window down")
map("n", "<C-S-k>", "<C-w>K", "move window up")
map("n", "<C-S-l>", "<C-w>L", "move window right")

-- resize window
map("n", "<M-h>", ":vertical resize +2<cr>", "resize left")
map("n", "<M-j>", ":resize -2<cr>", "resize down")
map("n", "<M-k>", ":resize +2<cr>", "resize up")
map("n", "<M-l>", ":vertical resize -2<cr>", "resize right")

-- line nav
-- Nav wrap lines
map("n", "j", [[(v:count > 5 ? "m'" . v:count : "") . 'gj']], "down", expr)
map("n", "k", [[(v:count > 5 ? "m'" . v:count : "") . 'gk']], "up", expr)

-- nav start/end of line
map("n", "gh", "^", "goto first char")
map("n", "gl", "$", "goto last char")
map("v", "gh", "^", "goto first char")
map("v", "gl", "$", "goto last char")

-- move line up/down
map("n", "<C-M-j>", ":m .+1<cr>==", "move line down")
map("n", "<C-M-k>", ":m .-2<cr>==", "move line up")
map("x", "<C-M-j>", ":move '>+2<cr>gv=gv", "move line down")
map("x", "<C-M-k>", ":move '<-2<cr>gv=gv", "move line up")

-- half screen jump
map("n", "<C-u>", "<C-u>zz", "center half page up")
map("n", "<C-d>", "<C-d>zz", "center half page down")

-- search

map("n", "*", ":keepjumps normal! mi*`i<CR>", "Star search")
map("n", "#", ":keepjumps normal! mi#`i<CR>", "Hash search")
map("n", "g*", ":keepjumps normal! mig*`i<CR>", "gStar search")
map("n", "g#", ":keepjumps normal! mig#`i<CR>", "gHash search")

-- find x in all files
mapl("n", "fv", ":vimgrep //g **/*<left><left><left><left><left><left><left>", "vim grep")

-- zoom/maximize

mapl("n", "mm", ":Maximize<cr>", "Maximize window")

-- marks

map("n", "<C-1>", "mA", "set mark 1")
map("n", "<C-2>", "mB", "set mark 2")
map("n", "<C-3>", "mC", "set mark 3")
map("n", "<C-4>", "mD", "set mark 4")
map("n", "<C-5>", "mE", "set mark 5")
mapl("n", "m1", "mA", "set mark 1")
mapl("n", "m2", "mB", "set mark 2")
mapl("n", "m3", "mC", "set mark 3")
mapl("n", "m4", "mD", "set mark 4")
mapl("n", "m5", "mE", "set mark 5")
mapl("n", "1", "`A", "goto 1st mark")
mapl("n", "2", "`B", "goto 2nd mark")
mapl("n", "3", "`C", "goto 3th mark")
mapl("n", "4", "`D", "goto 4th mark")
mapl("n", "5", "`E", "goto 5th mark")

-- quickfix list

mapl("n", "ql", ":call ToggleLocationList()<cr>", "Toggle locationlist", silent)
mapl("n", "qq", "<cmd>ToggleQuickFix<cr>", "Toggle quickfix", silent)

-- Diagnostics

-- add surounding symbol
map("v", '"', '<esc>`>a"<esc>`<i"<esc>', "add 2xquotes around selection")
map("v", "'", "<esc>`>a'<esc>`<i'<esc>", "add 1xquotes around selection")
map("v", "(", "<esc>`>a)<esc>`<i(<esc>", "add () around selection")
map("v", "{", "<esc>`>a}<esc>`<i{<esc>", "add {} around selection")
map("v", "[", "<esc>`>a]<esc>`<i[<esc>", "add [] around selection")

mapl("n", "df", vim.diagnostic.setqflist, "add Diagnostics to qf")

-- plugin - telescope
mapl("n", "fa", "<cmd>Telescope notify<cr>", "find notify msg")
mapl(
  "n",
  "bb",
  ImportExists(
    "telescope.builtin",
    function(builtin)
      builtin.buffers({ignore_current_buffer = true, sort_lastused = true})
    end
  ),
  "show buffers"
)
mapl(
  "n",
  "fb",
  ImportExists(
    "telescope.builtin",
    function(builtin)
      builtin.builtin()
    end
  ),
  "builtin"
)
map("n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", "hover doc", silent)
map("n", "<C-p>", "<cmd>lua vim.diagnostic.goto_prev()<CR>", "goto previous", silent)
map("n", "<C-n>", "<cmd>lua vim.diagnostic.goto_next()<CR>", "goto next", silent)
map(
  {"n", "v"},
  "gb",
  function()
    vim.lsp.buf.code_action(
      {
        filter = function(x)
          return x.kind == "quickfix"
        end,
        apply = true
      }
    )
  end,
  "code action fix",
  silent
)

-- plugin - tailwind
mapl("n", "cs", ":TailwindSort<cr>", "sorts all tailwind classes in the current buffer.")
mapl("v", "cs", ":TailwindSortSelection<cr>", "sorts selected tailwind classes in visual mode.")

-- git - git checkout neogit telescope gitblame
mapl(
  "n",
  "gb",
  ImportExists(
    "telescope.builtin",
    function(builtin)
      builtin.git_branches()
    end
  ),
  "branches"
)
mapl("n", "gg", ":Neogit<cr>", "git commit")
mapl(
  "n",
  "gv",
  ImportExists(
    "telescope.builtin",
    function(builtin)
      builtin.git_status()
    end
  ),
  "git status"
)
mapl(
  "n",
  "ga",
  ImportExists(
    "telescope.builtin",
    function(builtin)
      builtin.git_stash()
    end
  ),
  "git stash"
)
mapl("n", "go", ":Gitsigns toggle_current_line_blame<cr>", "Toggle git blame")
mapl(
  "n",
  "vv",
  ImportExists(
    "telescope.builtin",
    function(builtin)
      builtin.find_files(
        {
          search_dirs = {"$HOME/.dotfiles"},
          hidden = true,
          prompt_title = "< Config >"
        }
      )
    end
  ),
  "Edit config",
  silent
)
mapl(
  "n",
  "vg",
  ImportExists(
    "telescope.builtin",
    function(builtin)
      builtin.grep_string(
        {
          search_dirs = {"$HOME/.dotfiles"},
          hidden = true,
          prompt_title = "< Config >",
          search = vim.fn.input("C   ")
        }
      )
    end
  ),
  "grep config",
  silent
)
mapl(
  "n",
  "vn",
  ImportExists(
    "telescope.builtin",
    function(builtin)
      builtin.find_files(
        {
          search_dirs = {"$HOME/Documents/notes"},
          hidden = true,
          path_display = {"tail"},
          prompt_title = "< Notes >"
        }
      )
    end
  ),
  "Find notes",
  silent
)
mapl("n", "vr", "<cmd>e ~/Documents/notes/reg2025.md<cr>", "Open reg 2025", silent)
-- lsp

mapl(
  "n",
  "hi",
  function()
    vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
  end,
  "Toggle Inlay Hints"
)

vim.api.nvim_create_autocmd(
  "LspAttach",
  {
    callback = function(args)
      local bufnr = args.buf

      local silentBuf = {silent = true, buffer = bufnr}

      map("n", "gd", vim.lsp.buf.definition, "definition", silentBuf)
      map("n", "gD", vim.lsp.buf.declaration, "declaration", silentBuf)
      map("n", "K", vim.lsp.buf.hover, "hover", silentBuf)
      map("n", "gri", vim.lsp.buf.implementation, "implementation", silentBuf)
      map("n", "gs", vim.lsp.buf.signature_help, "signature", silentBuf)
      map("i", "<C-S>", vim.lsp.buf.signature_help, "signature", silentBuf)
      mapl("n", "wa", vim.lsp.buf.add_workspace_folder, "add_workspace_folder", silentBuf)
      mapl("n", "wr", vim.lsp.buf.remove_workspace_folder, "remove_workspace_folder", silentBuf)
      mapl(
        "n",
        "wl",
        function()
          print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
        end,
        "list_workspace_folders",
        silentBuf
      )
      map("n", "grt", vim.lsp.buf.type_definition, "type def", silentBuf)
      map("n", "grn", vim.lsp.buf.rename, "rename", silentBuf)
      mapl("n", "gra", vim.lsp.buf.code_action, "code action", silentBuf)
      map("n", "grr", vim.lsp.buf.references, "references", silentBuf)
      map(
        "n",
        "grf",
        function()
          vim.lsp.buf.format({async = true})
        end,
        "formatting",
        silentBuf
      )
    end
  }
)

-- database

mapl(
  "n",
  "od",
  function()
    vim.cmd(":tabnew")
    vim.cmd(":DBUI")
  end,
  "start db ui"
)
