local set = vim.opt
-- remove the last line in the popup
vim.cmd([[aunmenu PopUp.How-to\ disable\ mouse
          aunmenu PopUp.-1-]])

-- set filetypes for unknown files
vim.filetype.add(
  {
    extension = {
      mustache = "html"
    },
    filename = {
      [".babelrc"] = "json",
      [".eslintrc"] = "json",
      [".prettierrc"] = "json",
      [".jsbeautifyrc"] = "json"
    }
  }
)
local opts = vim.diagnostic.config()
opts.virtual_lines = true
opts.signs = {
  text = {
    [vim.diagnostic.severity.ERROR] = "",
    [vim.diagnostic.severity.WARN] = "",
    [vim.diagnostic.severity.INFO] = "",
    [vim.diagnostic.severity.HINT] = ""
  },
  numhl = {
    [vim.diagnostic.severity.ERROR] = "ErrorMsg",
    [vim.diagnostic.severity.WARN] = "WarningMsg",
    [vim.diagnostic.severity.INFO] = "DiagnosticInfo",
    [vim.diagnostic.severity.HINT] = "DiagnosticHint"
  }
}
vim.diagnostic.config(opts)

-- add filename and mod flag in the title
set.title = true
-- clipboard
set.clipboard = "unnamedplus"

-- stop pause on more screen
set.more = false

-- enable undo file - remember after reopen files
set.undofile = true

-- shared data options
set.shada = {"'10", "<0", "s10", "h"}

-- swap file options
set.swapfile = false

-- global statusline
set.cmdheight = 0

-- show matching bracket on insert
set.showmatch = true

-- line numbers
set.number = true
set.relativenumber = true

-- fix splits
set.splitbelow = true
set.splitright = true

-- fix the sign side bar
-- set.signcolumn = "yes"
set.numberwidth = 3
set.signcolumn = "yes:1"
set.statuscolumn = "%l%s"

-- default indent size
set.shiftwidth = 4
set.tabstop = 4
set.expandtab = true
set.shiftround = true

-- spelling
set.spell = false
set.spelllang = {"en_us", "da"}
set.spelloptions = "camel"

-- search options
set.smartcase = true
set.ignorecase = true

-- jump back the way you think it should c-o c-i
set.jumpoptions = "stack"

-- show the effect of a replace(:s) command in the buffer
set.inccommand = "split"

-- stop add more comment lines after comment lines
set.formatoptions = "tcqnljp"

-- short messages
set.shortmess = "loOtTcCF"
-- wrap lines
set.wrap = false
set.linebreak = true

-- folding
set.foldmethod = "indent"
set.foldenable = false

-- if there is rg on the system then grep with rg
if vim.fn.executable("rg") then
  set.grepprg = "rg --vimgrep --no-heading --hidden '$*'"
  set.grepformat = "%f:%l:%c:%m,%f:%l:%m"
end

-- special characters

-- change wrap end char
set.showbreak = "↪"
-- set unicode char for tab/nbsp/trail space ,eol:¬
set.listchars = {
  -- eol = '↲',
  tab = "▷⋯",
  leadmultispace = "┊   ",
  trail = "𝁢",
  extends = "»",
  precedes = "«",
  conceal = "¦",
  nbsp = "␣"
}
-- Visual Special Characters
set.fillchars = {
  eob = " ",
  diff = "⣿",
  vert = "│",
  fold = "·",
  foldsep = "│"
}
