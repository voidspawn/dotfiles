-- Functions
--- @param v unknown
function D(v)
  print(vim.inspect(v))
  return v
end

--- @param mode string|table
--- @param lhs string
--- @param rhs string|function
--- @param descr string
--- @param opts table|nil
function Keymap(mode, lhs, rhs, descr, opts)
  if opts == nil then
    opts = {desc = descr}
  else
    opts.desc = descr
  end
  vim.keymap.set(mode, lhs, rhs, opts)
end

--- @param mode string
--- @param lhs string
--- @param rhs string|function
--- @param descr string
--- @param opts table|nil
function Keymapl(mode, lhs, rhs, descr, opts)
  Keymap(mode, "<leader>" .. lhs, rhs, descr, opts)
end

-- Import

--- @param moduleName string
--- @param fn function
--- @return function
function ImportExists(moduleName, fn)
  return function()
    local ok, mod = pcall(require, moduleName)
    if ok then
      fn(mod)
    end
  end
end

--- @param moduleName string
--- @return nil|unknown
function Import(moduleName)
  if type(moduleName) ~= "string" then
    D("moduleName isn't a string")
    return nil
  end
  local ok, mod = pcall(require, moduleName)
  if not ok then
    D("Can't load module " .. moduleName)
    return nil
  end
  return mod
end

--- @param moduleName string
--- @param options function|table|nil
function ImportSetup(moduleName, options)
  local mod = Import(moduleName)
  if mod ~= nil then
    if type(options) == "function" then
      options(mod)
    elseif type(mod.setup) == "function" and type(options) == "table" then
      local ok, res = pcall(mod.setup, options)
      if not ok then
        vim.notify(res, "error", {title = "Error"})
      end
    elseif type(mod.setup) == "function" then
      local ok, res = pcall(mod.setup)
      if not ok then
        vim.notify(res, "error", {title = "Error"})
      end
    end
  end
end
