local function addDays(t, d)
  return t + (d * 24 * 60 * 60)
end
-- local function weekChar(t)
--   return string.upper(os.date("%a", t):sub(0, 1))
-- end
local function time2str(t)
  return os.date("%Y-%m-%d", t)
end

-- calctime
local function date()
  local linenr = vim.api.nvim_win_get_cursor(0)[1]
  local pos = vim.api.nvim_win_get_cursor(0)[2]
  local line = vim.api.nvim_get_current_line()
  -- get current date
  local time = os.time()
  -- get day of week
  local weekday = tonumber(os.date("%w"))

  -- sunday is last
  if weekday == 0 then
    weekday = 7
  end
  -- remove 1 for the current day
  weekday = weekday - 1

  -- get timestamp for monday
  local monday = addDays(time, -weekday)
  -- local modline = line:sub(0, pos) .. "## " .. os.date("%a"):sub(0, 1) .. os.date(" %Y-%m-%d") .. line:sub(pos + 1)
  -- string.upper(string)
  -- add the markdown header 2 with M 2020-06-15
  -- vim.api.nvim_set_current_line(modline)
  vim.api.nvim_buf_set_lines(0, linenr - 1, linenr, true, {
    line:sub(0, pos),
    "## M " .. time2str(monday),
    "",
    "601 634",
    "723 1357",
    "1429 1515",
    "1454 1540",
    "",
    "## T " .. time2str(addDays(monday, 1)),
    "",
    "530 1205",
    "",
    "## W " .. time2str(addDays(monday, 2)),
    "",
    "530 1204",
    "",
    "## T " .. time2str(addDays(monday, 3)),
    "",
    "601 634",
    "723 1357",
    "1429 1515",
    "1454 1540",
    "",
    "## F " .. time2str(addDays(monday, 4)),
    "",
    "601 634",
    "723 1357",
    "1429 1515",
    "1454 1540",
    line:sub(pos + 1),
  })
  -- jump into insert mode on next line
  -- vim.api.nvim_feedkeys("o", "n", true)
end

vim.keymap.set("n", "<leader>cd", date, { desc = "Add current date" })
-- vim.api.nvim_create_user_command("Date", date, {})

vim.keymap.set("n", "<leader>cu", ":update!<cr>|:!calctime --update '%'<cr>", { desc = "Calc time & update" })
-- calctime

-- functions
vim.api.nvim_create_user_command(
  "EmptyRegisters",
  function()
    local regs = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/-"'
    for r in regs:gmatch(".") do
      vim.fn.setreg(r, "")
    end
  end,
  {}
)
vim.api.nvim_create_user_command(
  "ToggleQuickFix",
  function()
    local qf_exists = false
    for _, win in pairs(vim.fn.getwininfo()) do
      if win["quickfix"] == 1 then
        qf_exists = true
      end
    end
    if qf_exists == true then
      vim.cmd("cclose")
      return
    end
    if not vim.tbl_isempty(vim.fn.getqflist()) then
      local ok, msg = pcall(vim.cmd, "copen")
      if not ok then
        vim.notify(msg, "error", {title = "Error: Quickfix"})
      end
    end
  end,
  {}
)

vim.cmd(
  [[
function! GetBufferify(cmd)
    let tmpfile = tempname()
    exec "redir > " . tmpfile
    exec "silent " . a:cmd
    redir END
    exec "view " . tmpfile
endfunc
]]
)

vim.cmd([[command! -nargs=1 Bufferify call GetBufferify(<f-args>)]])
vim.cmd([[command! -nargs=0 Messages call GetBufferify('messages')]])
vim.cmd([[command! -nargs=0 Maps call GetBufferify('map')]])
vim.cmd([[command! -nargs=0 Digraphs call GetBufferify('digraphs')]])
vim.cmd([[command! -nargs=0 Commands call GetBufferify('command')]])
vim.cmd([[command! -nargs=0 Highlight call GetBufferify('highlight')]])
