local autocmd = vim.api.nvim_create_autocmd
local augroup = function(name)
  return vim.api.nvim_create_augroup(name, {clear = true})
end
local ol = vim.opt_local

-- disables automatic commenting on newline
autocmd(
  "FileType",
  {
    group = augroup("automatic"),
    pattern = "*",
    callback = function()
      ol.formatoptions = "jqltn"
      -- ol.formatoptions:remove("c")
      -- ol.formatoptions:remove("r")
      -- ol.formatoptions:remove("o")
    end
  }
)

-- Automatically deletes all trailing whitespace on save
autocmd(
  "BufWritePre",
  {
    group = augroup("trailing_whitespace"),
    callback = function()
      local save_cursor = vim.fn.getpos(".")
      vim.cmd([[%s/\s\+$//e]])
      vim.fn.setpos(".", save_cursor)
    end
  }
)

-- run zrdb whenever Xdefaults or Xresources are updated.
autocmd(
  "BufWritePost",
  {
    group = augroup("trimWS"),
    pattern = {".Xresources", ".Xdefaults"},
    callback = function()
      vim.cmd("!xrdb %")
    end
  }
)

-- read file if change outside of vim
autocmd(
  "VimResized",
  {
    group = augroup("resize_splits"),
    pattern = "*",
    callback = function()
      vim.cmd(":wincmd =")
    end
  }
)

-- Highlight Yanked String
autocmd(
  "TextYankPost",
  {
    group = augroup("highlight_yank"),
    callback = function()
      vim.highlight.on_yank({timeout = 400})
    end
  }
)

autocmd(
  "BufWinEnter",
  {
    group = augroup("help_local_mappings"),
    pattern = {"*"},
    callback = function(event)
      if vim.bo.buftype == "help" then
        vim.keymap.set("n", "<CR>", "<C-]>", {buffer = event.buf, silent = true})
        vim.keymap.set("n", "<BS>", "<C-t>", {buffer = event.buf, silent = true})
      end
    end
  }
)

-- vim.cmd("autocmd QuickFixCmdPost [^l]* cwindow")
-- vim.cmd("autocmd QuickFixCmdPost l* lwindow")

autocmd(
  "BufReadPost",
  {
    group = augroup("last_loc"),
    callback = function()
      local mark = vim.api.nvim_buf_get_mark(0, '"')
      local lcount = vim.api.nvim_buf_line_count(0)
      if mark[1] > 0 and mark[1] <= lcount then
        pcall(vim.api.nvim_win_set_cursor, 0, mark)
      end
    end
  }
)

autocmd(
  "FileType",
  {
    group = augroup("close_with_q"),
    pattern = {
      "checkhealth",
      "fugitive*",
      "git",
      "help",
      "lspinfo",
      "man",
      "netrw",
      "nofile",
      "notify",
      "PlenaryTestPopup",
      "qf",
      "query",
      "spectre_panel",
      "startuptime",
      "tsplayground"
    },
    callback = function(event)
      vim.bo[event.buf].buflisted = false
      vim.keymap.set("n", "q", vim.cmd.close, {desc = "Close the current buffer", buffer = true, silent = true})
    end
  }
)

vim.cmd([[command DiffOrig vert new | set buftype=nofile | read ++edit # | 0d_ | diffthis | wincmd p | diffthis]])
