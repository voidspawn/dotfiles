# This is an example Hyprland config file.
#
# Refer to the wiki for more information.

#
# Please note not all available settings / options are set here.
# For a full list, see the wiki
#

# See https://wiki.hyprland.org/Configuring/Monitors/
monitor = ,preferred,auto,auto
monitor=DP-1,3840x2160@60,1920x0,2, bitdepth, 10
# monitor=DP-1,1920x1080@60,1920x0,1
monitor=HDMI-A-1,1920x1080@60,0x0,1

# Work
monitor=eDP-1,1920x1200@60,0x0,1
monitor=DP-7,2560x1440@60,1920x0,1
monitor=DP-6,2560x1440@60,4480x0,1
monitor=DP-9,2560x1440@60,1920x0,1
monitor=DP-8,2560x1440@60,4480x0,1

exec-once = waybar
exec-once = dunst
exec-once = hypridle

# exec = wlr-randr --output HDMI-A-1 --mode 1920x1080 --pos 0,0 --output DP-1 --mode 3840x2160 --pos 1920,0 --scale 2

exec-once = bluetoothctl power on
exec-once = bluetooth-autoconnect -d &
exec-once = pidof -o %PPID -x blueman-applet >/dev/null || (sleep 2s && blueman-applet &)

# exec-once = kdeconnect-indicator &
exec-once = /usr/bin/kdeconnectd &

# mount applet
exec-once = pidof -o %PPID -x udiskie >/dev/null || (sleep 2s && udiskie --tray &)

# Sound applet
exec-once = pidof -o %PPID -x pnmixer >/dev/null || (sleep 2s && pnmixer &)
exec-once = pidof -o %PPID -x nm-applet >/dev/null || (sleep 2s && nm-applet --indicator &)


# hotkeys
#pidof -o %PPID -x sxhkd >/dev/null || (sxhkd -c ~/.config/i3/sxhkdrc &)

exec-once = pidof -o %PPID -x dropbox >/dev/null || (sleep 5s && dropbox &)

#sleep 1s && rrandr &
exec-once = /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
#exec-once = /usr/lib/polkit-kde-authentication-agent-1
exec-once = xhost + local:
exec-once = xhost +si:localuser:$USER &
# background images
#sleep 1s && nitrogen --restore &
# feh --bg-scale --no-fehbg ~/.config/bg.jpg
exec-once = wbg ~/.config/bg.png
# srandrd <- can be use to load the bar when new screen is added

exec-once = dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec-once = $HOME/.config/hypr/autorun.sh

# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
# exec-once = waybar & hyprpaper & firefox

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# Some default env vars.
env = XCURSOR_SIZE,24
env = QT_QPA_PLATFORMTHEME,qt5ct
env = QT_STYLE_OVERRIDE,kvantum
env = MOZ_ENABLE_WAYLAND,1
env = HYPRCURSOR_THEME,Oxygen_White
env = HYPRCURSOR_SIZE,24

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = dk
    kb_options = caps:escape

    follow_mouse = 1

    touchpad {
        natural_scroll = true
        clickfinger_behavior = true
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 5
    gaps_out = 20
    border_size = 1
    #col.inactive_border = rgba(1f2335aa)
    #col.active_border = rgba(3b4261ee)
    # col.inactive_border = rgba(595959aa)
    # col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg

    col.active_border = 0xff7aa2f7
    # col.active_border = 0xff5eac81
    col.inactive_border = 0x66333333

    #layout = dwindle
    layout = master
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 10
    blur {
        enabled = true
        size = 2
        passes = 3
        new_optimizations = true
    }

    shadow {
        enabled = true
        range = 30
        # render_power = 3
        # color = rgba(1a1a1aee)
        # color = 0xffa7caff #86aaec
        # color = 0xdf87aaff #466a9c
        # color = 0xdf87ffaa #469c6a
        # color = 0x4faa87ff #9c466a
        color = 0x8f87aaff #466a9c
        color_inactive = 0x50000000
    }
}

# blur waybar
blurls=waybar

animations {
    enabled = true

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 7, myBezier
    animation = windowsOut, 1, 7, default, popin 80%
    animation = border, 1, 10, default
    animation = borderangle, 1, 8, default
    animation = fade, 1, 7, default
    animation = workspaces, 1, 6, default
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = true # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = true # you probably want this
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    allow_small_split = false
    new_status = slave
    new_on_top = true
    inherit_fullscreen = true
    smart_resizing = true
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = true
    #workspace_swipe_min_speed_to_force = 4
}

# Example per-device config
# See https://wiki.hyprland.org/Configuring/Keywords/#executing for more
device {
    name = epic-mouse-v1
    sensitivity = -0.5
}

misc {
    disable_hyprland_logo = true
    disable_splash_rendering = true
    vfr = true
    vrr = 0
    mouse_move_enables_dpms = true
    enable_swallow = 1
    swallow_regex=^(kitty|Alacritty|ghostty)$
}

binds {
    workspace_back_and_forth = true
}

render {
    direct_scanout = false
}

#debug {
#    damage_tracking = 2
#}
# Example windowrule v1
# windowrule = float, ^(kitty)$
# Example windowrule v2
# windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
workspace = w[tv1], gapsout:0, gapsin:0
workspace = f[1], gapsout:0, gapsin:0
windowrulev2 = bordersize 0, floating:0, onworkspace:w[tv1]
windowrulev2 = rounding 0, floating:0, onworkspace:w[tv1]
windowrulev2 = bordersize 0, floating:0, onworkspace:f[1]
windowrulev2 = rounding 0, floating:0, onworkspace:f[1]



windowrule = tile, ^(kitty)$
windowrule = float,class:^(steam_app_32370)$
windowrule = float,class:^(steam_app_507490)$
windowrule = float,class:^(steam_app_1182480)$
windowrule = float,class:^(steam_app_1794680)$
windowrule = float,class:^(steam_app_489830)$
# BG 3
windowrulev2 = monitor DP-1, class:steam_app_1086940, title:LariLauncher
# fallout 4
windowrule = float,class:^(steam_app_377160)$
windowrule = float,class:^(Paradox Launcher)$
# rofi
windowrulev2 = stayfocused, class:(Rofi)$
#windowrulev2 = forceinput, class:(Rofi)$

layerrule = blur,waybar
layerrule = blur,wofi
layerrule = blur,rofi
# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod, RETURN, exec, ghostty
bind = $mainMod SHIFT, RETURN, layoutmsg, swapwithmaster
bind = $mainMod, E, exec, dolphin
bind = $mainMod, N, togglefloating,
# bind = $mainMod, X, exec, wofi --show "drun,run" -p "What is do you need?" -G -I -i
# bind = $mainMod, SPACE, exec, rofi -modi combi -show combi -show-icons -combi-modes "drun,run"
bind = $mainMod, SPACE, exec, wofi --show "drun,run" -p "What is do you need?" -G -I -i
bind = CTRL ALT, DELETE, exec, kitty -T powermenu -e powermenu
bind = $mainMod, P, pseudo, # dwindle
bind = $mainMod, V, togglesplit, # dwindle
bind = $mainMod, F, fullscreen,
bind = $mainMod, M, fullscreen, 1
bind = $mainMod SHIFT, P, exec, grim -g "$(slurp)"

bind = CTRL ALT, escape, exec, hyprctl kill

bind = SUPER, Q, submap, quitsubmaps
submap = quitsubmaps
bind = ,Q,killactive,
bind = ,Q,submap,reset
bind = ,P,exec, hyprctl activewindow -j |jq -r ".class"| wl-copy
bind = ,P,submap,reset
bind = ,M,exit,
bind = ,M,submap,reset
bind = ,L,exec,wlogout,
bind = ,L,submap,reset
bind = ,ESCAPE,submap,reset
submap = reset

bind = SUPER, R, submap, runsubmaps
submap = runsubmaps
bind = ,S,exec,steam
bind = ,S,submap,reset
bind = ,B,exec,firefox
bind = ,B,submap,reset
bind = ,U,exec,click4ever
bind = ,U,submap,reset
bind = ,I,exec,click4ever --button=right
bind = ,I,submap,reset
bind = ,C,exec,kitty -T Cheats -e searchcht
bind = ,C,submap,reset
bind = ,E,exec,ibus emoji
bind = ,E,submap,reset
bind = ,P,exec,colorpicker --short --one-shot --preview | xclip -selection clipboard
bind = ,P,submap,reset
bind = ,R,exec,hyprctl reload
bind = ,R,submap,reset
bind = ,F,exec,kitty -T FileManager -e lf
bind = ,F,submap,reset
bind = ,N,exec,kitty -T NewsBoat -e newsboat
bind = ,N,submap,reset
bind = ,Y,exec,plplay
bind = ,Y,submap,reset
bind = ,ESCAPE,submap,reset
submap = reset

# Move focus with mainMod + arrow keys
bind = $mainMod, H, movefocus, l
bind = $mainMod, L, movefocus, r
bind = $mainMod, K, movefocus, u
bind = $mainMod, J, movefocus, d

# Move window with mainMod + arrow keys
bind = $mainMod SHIFT, H, movewindow, l
bind = $mainMod SHIFT, L, movewindow, r
bind = $mainMod SHIFT, K, movewindow, u
bind = $mainMod SHIFT, J, movewindow, d

# Resize window with mainMod + arrow keys
bind = $mainMod CTRL, H, resizeactive, -40 0
bind = $mainMod CTRL, L, resizeactive, 40 0
bind = $mainMod CTRL, K, resizeactive, 0 -40
bind = $mainMod CTRL, J, resizeactive, 0 40

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
bind = $mainMod SHIFT, 0, movetoworkspace, 10

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow
