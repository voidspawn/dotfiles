#!/usr/bin/env bash

# transparent terminal
launch_picom.sh &


# hotkeys
# killall sxhkd
# pidof -o %PPID -sxhkd >/dev/null || (sxhkd -c ~/.config/i3/sxhkdrc) &

rrandr &


~/.config/polybar/launch.sh &

# background images
sleep 1s && nitrogen --restore &
