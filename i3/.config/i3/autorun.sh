#!/usr/bin/env bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}
# To enable the screen saver only after 15 minutes (900 seconds) idle time
xset s 900
# To turn off the monitor after 20 minutes of idling run
xset dpms 0 0 1200

# map caps to esc
setxkbmap -option caps:escape -layout dk &

# polkit
/usr/lib/polkit-kde-authentication-agent-1&

# power on bluetooth
bluetoothctl power on
bluetooth-autoconnect -d &
pidof -o %PPID -x blueman-applet >/dev/null || (sleep 2s && blueman-applet &)

pidof -o %PPID -x kdeconnect-indicator >/dev/null || (sleep 5s && kdeconnect-indicator &)
# pidof -o %PPID -x nextcloud >/dev/null || (nextcloud --background&)

# mount applet
pidof -o %PPID -x udiskie >/dev/null || (sleep 2s && udiskie --tray &)

# Sound applet
pidof -o %PPID -x pnmixer >/dev/null || (sleep 2s && pnmixer &)
pidof -o %PPID -x nm-applet >/dev/null || (sleep 2s && nm-applet --indicator &)

# hotkeys
pidof -o %PPID -x sxhkd >/dev/null || (sleep 2s && sxhkd -c ~/.config/i3/sxhkdrc &)

pidof -o %PPID -x dropbox >/dev/null || (sleep 5s && dropbox &)

# feh --bg-scale --no-fehbg ~/.config/bg.jpg

# srandrd <- can be use to load the bar when new screen is added

sleep 2s && ~/.config/i3/restart.sh
