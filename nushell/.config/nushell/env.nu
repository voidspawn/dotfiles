# Nushell Environment Config File
#
# version = "0.99.1"
def concat [path1, path2] {
    return ($path1 | append $path2 | str join "/")
}
def create_left_prompt [] {
    let dir = match (do --ignore-shell-errors { $env.PWD | path relative-to $nu.home-path }) {
        null => $env.PWD
        '' => '~'
        $relative_pwd => ([~ $relative_pwd] | path join)
    }

    let path_color = (if (is-admin) { ansi red_bold } else { ansi green_bold })
    let separator_color = (if (is-admin) { ansi light_red_bold } else { ansi light_green_bold })
    let path_segment = $"($path_color)($dir)(ansi reset)"

    $path_segment | str replace --all (char path_sep) $"($separator_color)(char path_sep)($path_color)"
}

def create_right_prompt [] {
    # create a right prompt in magenta with green separators and am/pm underlined
    let time_segment = ([
        (ansi reset)
        (ansi magenta)
        (date now | format date '%x %X') # try to respect user's locale
    ] | str join | str replace --regex --all "([/:])" $"(ansi green)${1}(ansi magenta)" |
        str replace --regex --all "([AP]M)" $"(ansi magenta_underline)${1}")

    let last_exit_code = if ($env.LAST_EXIT_CODE != 0) {([
        (ansi rb)
        ($env.LAST_EXIT_CODE)
    ] | str join)
    } else { "" }

    ([$last_exit_code, (char space), $time_segment] | str join)
}

# Use nushell functions to define your right and left prompt
$env.PROMPT_COMMAND = {|| create_left_prompt }
# FIXME: This default is not implemented in rust code as of 2023-09-08.
$env.PROMPT_COMMAND_RIGHT = {|| create_right_prompt }

# The prompt indicators are environmental variables that represent
# the state of the prompt
$env.PROMPT_INDICATOR = {|| "> " }
$env.PROMPT_INDICATOR_VI_INSERT = {|| ": " }
$env.PROMPT_INDICATOR_VI_NORMAL = {|| "> " }
$env.PROMPT_MULTILINE_INDICATOR = {|| "::: " }

# If you want previously entered commands to have a different prompt from the usual one,
# you can uncomment one or more of the following lines.
# This can be useful if you have a 2-line prompt and it's taking up a lot of space
# because every command entered takes up 2 lines instead of 1. You can then uncomment
# the line below so that previously entered commands show with a single `🚀`.
# $env.TRANSIENT_PROMPT_COMMAND = {|| "🚀 " }
# $env.TRANSIENT_PROMPT_INDICATOR = {|| "" }
# $env.TRANSIENT_PROMPT_INDICATOR_VI_INSERT = {|| "" }
# $env.TRANSIENT_PROMPT_INDICATOR_VI_NORMAL = {|| "" }
# $env.TRANSIENT_PROMPT_MULTILINE_INDICATOR = {|| "" }
# $env.TRANSIENT_PROMPT_COMMAND_RIGHT = {|| "" }

# Specifies how environment variables are:
# - converted from a string to a value on Nushell startup (from_string)
# - converted from a value back to a string when running external commands (to_string)
# Note: The conversions happen *after* config.nu is loaded
$env.ENV_CONVERSIONS = {
    "PATH": {
        from_string: { |s| $s | split row (char esep) | path expand --no-symlink }
        to_string: { |v| $v | path expand --no-symlink | str join (char esep) }
    }
    "Path": {
        from_string: { |s| $s | split row (char esep) | path expand --no-symlink }
        to_string: { |v| $v | path expand --no-symlink | str join (char esep) }
    }
}

# Directories to search for scripts when calling source or use
# The default for this is $nu.default-config-dir/scripts
$env.NU_LIB_DIRS = [
    ($nu.default-config-dir | path join 'scripts') # add <nushell-config-dir>/scripts
    ($nu.data-dir | path join 'completions') # default home for nushell completions
]

# Directories to search for plugin binaries when calling register
# The default for this is $nu.default-config-dir/plugins
$env.NU_PLUGIN_DIRS = [
    ($nu.default-config-dir | path join 'plugins') # add <nushell-config-dir>/plugins
]

# To add entries to PATH (on Windows you might use Path), you can use the following pattern:
# $env.PATH = ($env.PATH | split row (char esep) | prepend '/some/path')
# An alternate way to add entries to $env.PATH is to use the custom command `path add`
# which is built into the nushell stdlib:
# use std "path add"
# $env.PATH = ($env.PATH | split row (char esep))
# path add /some/path
# path add ($env.CARGO_HOME | path join "bin")
# path add ($env.HOME | path join ".local" "bin")
# $env.PATH = ($env.PATH | uniq)

# To load from a custom file you can use:
# source ($nu.default-config-dir | path join 'custom.nu')

$env.TERM = 'xterm-256color'
$env.GTK_THEME = 'Breeze:dark'
$env.STOW_DIR = concat $env.HOME '.dotfiles'
$env.EDITOR = 'nvim'
$env.SUDO_EDITOR = 'nvim'
$env.TERMINAL = 'ghostty'
$env.BROWSER = 'firefox'
# $env.MANPAGER = "sh -c 'col -bx | bat -l man -p'"
$env.MANPAGER = "nvim +Man!"
$env.QT_STYLE_OVERRIDE = 'kvantum'

$env.XDG_CONFIG_HOME = concat $env.HOME '.config'
$env.XDG_DATA_HOME =  concat $env.HOME '.local/share'
$env.XDG_CACHE_HOME = concat $env.HOME '.cache'
$env.XDG_STATE_HOME = concat $env.HOME '.local/state'

$env.GTK2_RC_FILES = concat $env.XDG_CONFIG_HOME 'gtk-2.0/gtkrc'
$env.WGETRC = concat $env.XDG_CONFIG_HOME 'wget/wgetrc'
# $env.INPUTRC = concat $env.XDG_CONFIG_HOME 'shell/inputrc'
$env.ZDOTDIR = concat $env.XDG_CONFIG_HOME 'zsh'
$env.WINEPREFIX = concat $env.XDG_DATA_HOME 'wineprefixes/default'
$env.KODI_DATA = concat $env.XDG_DATA_HOME 'kodi'
$env.PASSWORD_STORE_DIR = concat $env.XDG_DATA_HOME 'password-store'
$env.TMUX_TMPDIR = $env.XDG_RUNTIME_DIR
$env.ANDROID_SDK_HOME = concat $env.XDG_CONFIG_HOME 'android'
$env.ANSIBLE_CONFIG = concat $env.XDG_CONFIG_HOME 'ansible/ansible.cfg'
$env.UNISON = concat $env.XDG_DATA_HOME 'unison'
$env.HISTFILE = concat $env.XDG_DATA_HOME 'history'
$env.WEECHAT_HOME = concat $env.XDG_CONFIG_HOME 'weechat'
$env.MBSYNCRC = concat $env.XDG_CONFIG_HOME 'mbsync/config'
$env.ELECTRUMDIR = concat $env.XDG_DATA_HOME 'electrum'

$env.GNUPGHOME = concat $env.XDG_DATA_HOME 'gnupg'
$env.KDEHOME = concat $env.XDG_CONFIG_HOME 'kde'
# $env.LESSHISTFILE = "-"
$env.LESSHISTFILE = concat $env.XDG_CACHE_HOME 'less/history'
$env.MPLAYER_HOME = concat $env.XDG_CONFIG_HOME 'mplayer'

$env.XINITRC = concat $env.XDG_CONFIG_HOME 'X11/xinitrc'
$env._Z_DATA = concat $env.XDG_DATA_HOME 'z'

$env.Path = ($env.Path | prepend 'bin')
$env.Path = ($env.Path | prepend '~/bin')
$env.Path = ($env.Path | prepend '~/.local/bin')



# Deno
$env.DENO_INSTALL = '~/.deno'
$env.DENO_INSTALL_BIN = '~/.deno/bin'
$env.Path = ($env.Path | prepend $env.DENO_INSTALL_BIN)



# NodeJS
$env.Path = ($env.Path | prepend '~/.local/share/npm/bin')
# $env.Path = node_modules/.bin $PATH
$env.NODE_OPTIONS = "--max-old-space-size=8192"

#npm set this
#npm config set prefix=${XDG_DATA_HOME}/npm cache=${XDG_CACHE_HOME}/npm init-module=${XDG_CONFIG_HOME}/npm/config/npm-init.js
$env.NPM_CONFIG_USERCONFIG = concat $env.XDG_CONFIG_HOME 'npm/npmrc'

# Java
$env._JAVA_OPTIONS = '-Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java'

# Go
$env.GOPATH = concat $env.XDG_DATA_HOME 'go'
$env.GOBIN = concat $env.XDG_DATA_HOME 'go/bin'
# set -g GOPATH /go
$env.Path = ($env.Path | prepend $env.GOBIN)

# Rust
$env.CARGO_HOME = concat $env.XDG_DATA_HOME 'cargo'
$env.CARGO_BIN = concat $env.XDG_DATA_HOME 'cargo/bin'
$env.Path = ($env.Path | prepend $env.CARGO_BIN )


# ssh
$env.SUDO_ASKPASS = '/usr/bin/lxqt-openssh-askpass &'



# completions
$env.CARAPACE_BRIDGES = 'zsh,fish,bash,inshellisense' # optional
mkdir ~/.cache/carapace
carapace _carapace nushell | save --force ~/.cache/carapace/init.nu

# prompt
$env.STARSHIP_CONFIG = concat $env.HOME '.config/starship/starship.toml'
mkdir ~/.cache/starship
starship init nu | save -f ~/.cache/starship/init.nu

# zoxide - z nav.
mkdir ~/.cache/zoxide
zoxide init nushell | save -f ~/.cache/zoxide/init.nu
